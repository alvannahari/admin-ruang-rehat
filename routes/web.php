<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

Route::group(['middleware' => ['auth:web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('notifications', 'HomeController@notifications');

    Route::get('feed', 'FeedController@index')->name('feed');
    Route::get('getAllfeeds', 'FeedController@getAllfeeds');
    Route::get('feed/{feed}', 'FeedController@show');
    Route::get('feed/{feed}/delete', 'FeedController@destroy');
    Route::get('feedComment/{feedComment}/delete', 'FeedController@destroyComment');
    
    Route::get('magazine', 'MagazineController@index')->name('magazine');
    Route::get('getAllMagazine', 'MagazineController@getAllMagazine');
    Route::post('magazine', 'MagazineController@store');
    Route::get('magazine/{magazine}/delete', 'MagazineController@destroy');
    Route::get('magazine/{magazine}/{status}', 'MagazineController@status');
    Route::get('magazine/{magazine}', 'MagazineController@show');
    Route::post('magazine/{magazine}', 'MagazineController@update');
    Route::get('magazineComment/{magazineComment}/delete', 'MagazineController@destroyComment');
    Route::get('magazineComment/{magazineComment}/{status}', 'MagazineController@updateComment');

    Route::get('user', 'UserController@index')->name('user');
    Route::get('getAllUser', 'UserController@getAllUser');
    Route::get('user/{user}/delete', 'UserController@destroy');
    Route::get('user/{user}', 'UserController@show');

    Route::get('diary/{diary}/delete', 'DiaryController@destroy');
    
    Route::get('report', 'ReportController@index');
    Route::get('report/{report}', 'ReportController@show');
    Route::get('reportFeed/{feed}', 'ReportController@reportFeed');
    Route::get('reportFeedComment/{feedComment}', 'ReportController@reportFeedComment');
    Route::get('report/{report}/delete', 'ReportController@destroy');

    // Route::get('admin', 'AdminController@index')->name('admin');

});
