<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('error', function () {
    return response([
        'status'    => false,
        'message'   => 'Request Unauthorized'
    ], 401);
})->name('error');

Route::get('/', function() {
    return response([
        'app' => 'API_Rumah_Rehat',
        'version' => 'v1.0'
    ], 200);
});

Route::prefix('user')->group( function () {
    Route::post('login', 'Api\V1\User\Login');
    Route::post('register', 'Api\V1\User\Register');
});

Route::middleware('auth:api')->group( function() {
    Route::prefix('user')->group( function () {
        Route::post('changeProfile', 'Api\V1\User\ChangeProfile');
        Route::get('getDetail', 'Api\V1\User\GetDetail');
        Route::get('getUser', 'Api\V1\User\GetUser');
        Route::get('getAllUsers', 'Api\V1\User\GetAllUsers');
        Route::get('follow', 'Api\V1\User\Follow');
        Route::get('unfollow', 'Api\V1\User\Unfollow');
    });
    
    Route::prefix('magazine')->group(function() {
        Route::post('createMagazine', 'Api\V1\Magazine\CreateMagazine');
        Route::get('getAllMagazine', 'Api\V1\Magazine\GetAllMagazine');
        Route::get('getDetail', 'Api\V1\Magazine\GetDetail');
        Route::get('giveLike', 'Api\V1\Magazine\GiveLike');
        Route::get('unlike', 'Api\V1\Magazine\Unlike');
        Route::get('delete', 'Api\V1\Magazine\Delete');
        Route::post('leaveComment', 'Api\V1\Magazine\LeaveComment');
    });
    
    Route::prefix('diary')->group(function() {
        Route::get('getDiaryByUser', 'Api\V1\Diary\GetDiaryByUser');
        Route::post('addDiary', 'Api\V1\Diary\AddDiary');
        Route::get('delete', 'Api\V1\Diary\Delete');
        Route::get('getDetail', 'Api\V1\Diary\GetDetail');
        Route::post('updateDiary', 'Api\V1\Diary\UpdateDiary');
    });
    
    Route::prefix('feed')->group(function() {
        Route::get('getAllPostByUser', 'Api\V1\Feed\GetAllPostByUser');
        Route::get('getDetail', 'Api\V1\Feed\GetDetail');
        Route::get('delete', 'Api\V1\Feed\Delete');
        Route::post('addPost', 'Api\V1\Feed\AddPost');
        Route::post('leaveComment', 'Api\V1\Feed\LeaveComment');
        Route::post('report', 'Api\V1\Feed\ReportFeed');
        Route::post('reportComment', 'Api\V1\Feed\ReportComment');
    });
    
});
