<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Feed;
use App\Models\FeedComment;

class CreateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->id();
            $table->string(Feed::USER_ID);
            $table->mediumText(Feed::CONTENT);
            $table->enum(Feed::IS_ANONYM, [0,1])->default(0);
            $table->enum(Feed::IS_REPORTED, [0,1])->default(0);
            $table->timestamps();
        });
        Schema::create('feed_comments', function (Blueprint $table) {
            $table->id();
            $table->string(FeedComment::FEED_ID);
            $table->string(FeedComment::USER_ID);
            $table->string(FeedComment::COMMENT);
            $table->enum(FeedComment::IS_REPORTED, [0,1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeds');
        Schema::dropIfExists('feed_comments');
    }
}
