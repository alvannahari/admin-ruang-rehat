<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Magazine;
use App\Models\MagazineComment;
use App\Models\MagazineLike;

class CreateMagazinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('magazines', function (Blueprint $table) {
            $table->id();
            $table->string(Magazine::USER_ID);
            $table->enum(Magazine::IS_ADMIN, [0,1])->default(1);
            $table->string(Magazine::TITLE);
            $table->longText(Magazine::CONTENT);
            $table->string(Magazine::PHOTO)->nullable()->default('default.jpg');
            $table->enum(Magazine::IS_APPROVE, [0,1])->default(1);
            $table->timestamps();
        });
        Schema::create('magazine_comments', function (Blueprint $table) {
            $table->id();
            $table->string(MagazineComment::MAGAZINE_ID);
            $table->string(MagazineComment::USER_ID);
            $table->string(MagazineComment::COMMENT);
            $table->enum(MagazineComment::IS_APPROVE, [0,1])->default(0);
            $table->timestamps();
        });
        Schema::create('magazine_likes', function (Blueprint $table) {
            $table->id();
            $table->string(MagazineLike::MAGAZINE_ID);
            $table->string(MagazineLike::USER_ID);
            $table->timestamp(MagazineLike::CREATED_AT)->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magazines');
    }
}
