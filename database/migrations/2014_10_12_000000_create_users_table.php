<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;
use App\Models\Admin;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string(User::FULLNAME);
            $table->string(User::EMAIL)->unique();
            $table->string(User::USERNAME)->unique();
            $table->string(User::PASSWORD);
            $table->string(User::PHONE);
            $table->string(User::PHOTO)->nullable();
            $table->timestamps();
        });
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string(Admin::EMAIL)->unique();
            $table->string(Admin::PASSWORD);
            $table->string(Admin::VERIFIED_AT)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('admins');
    }
}
