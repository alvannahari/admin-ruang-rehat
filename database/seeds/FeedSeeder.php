<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Feed;
use App\Models\FeedComment;

class FeedSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = 1;
        for ($i=1; $i < 100 ; $i++) {
            DB::table('feeds')->insert([
                Feed::USER_ID       => $i%6 == 0 ? $id++ : $id,
                Feed::CONTENT       => 'Feed atau post ke '.$i.'
                Deskripsi lanjutan untuk post ini....',
                Feed::IS_ANONYM     => 1,
                Feed::CREATED_AT    => now(),
                Feed::UPDATED_AT    => now()
            ]);
        }
        $idUser = 1;
        $idFeed = 1;
        for ($i=1; $i < 50 ; $i++) {
            DB::table('feed_comments')->insert([
                FeedComment::COMMENT       => 'Komentar untuk feed atau post '.$idFeed.' dari user '.$idUser,
                FeedComment::FEED_ID       => $i%5 == 0 ? $idFeed++ : $idFeed,
                FeedComment::USER_ID       => $i%4 == 0 ? $idUser++ : $idUser,
                FeedComment::CREATED_AT    => now(),
                FeedComment::UPDATED_AT    => now()
            ]);
        }
    }
}
