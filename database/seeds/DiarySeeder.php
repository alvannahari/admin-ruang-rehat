<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Diary;

class DiarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = 1;
        for ($i=1; $i < 10 ; $i++) {
            DB::table('diaries')->insert([
                Diary::USER_ID       => $i%3 == 0 ? $idUser++ : $idUser,
                Diary::TITLE         => 'Title Diary '.$i,
                Diary::DIARY       => 'Diary ke '.$i.'
Deskripsi lanjutan untuk Diary ini....
Foto lanjutan untuk Diary ini....',
                Diary::IS_PUBLIC    => 1,
                Diary::CREATED_AT    => now(),
                Diary::UPDATED_AT    => now()
            ]);
        }
    }
}
