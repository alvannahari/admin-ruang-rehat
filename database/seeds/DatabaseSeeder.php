<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(FeedSeeder::class);
        // $this->call(MagazineSeeder::class);
        // $this->call(DiarySeeder::class);
    }
}
