<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Magazine;
use App\Models\MagazineComment;

class MagazineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 10 ; $i++) {
            DB::table('magazines')->insert([
                Magazine::USER_ID       => '1',
                Magazine::IS_ADMIN      => '1',
                Magazine::TITLE         => 'Title Magazine '.$i,
                Magazine::CONTENT       => 'Magazine ke '.$i.'
Deskripsi lanjutan untuk Magazine ini....
Foto lanjutan untuk magazine ini....',
                Magazine::IS_APPROVE    => '1',
                Magazine::CREATED_AT    => now(),
                Magazine::UPDATED_AT    => now()
            ]);
        }
        $idUser = 1;
        $idMagazine = 1;
        for ($i=1; $i < 15 ; $i++) {
            DB::table('magazine_comments')->insert([
                MagazineComment::COMMENT       => 'Komentar untuk Magazine '.$idMagazine.' dari user '.$idUser,
                MagazineComment::MAGAZINE_ID   => $i%2 == 0 ? $idMagazine++ : $idMagazine,
                MagazineComment::USER_ID       => $i%3 == 0 ? $idUser++ : $idUser,
                MagazineComment::IS_APPROVE    => '1',
                MagazineComment::CREATED_AT    => now(),
                MagazineComment::UPDATED_AT    => now()
            ]);
        }
    }
}
