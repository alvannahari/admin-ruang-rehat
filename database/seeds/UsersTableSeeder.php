<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use App\Models\User;
use App\Models\Admin;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create('id_ID');

        for ($i=1; $i < 6 ; $i++) {
            DB::table('users')->insert([
                User::FULLNAME         => 'user test 0'.$i,
                User::EMAIL         => 'user0'.$i.'@test.com',
                User::USERNAME      => 'user0'.$i,
                User::PASSWORD      => bcrypt('user0'.$i),
                User::PHONE         => $faker->phoneNumber,
                User::CREATED_AT    => now(),
                User::UPDATED_AT    => now()
            ]);
        }

        for ($i=1; $i < 3 ; $i++) {
            DB::table('admins')->insert([
                Admin::EMAIL         => 'admin0'.$i.'@test.com',
                Admin::PASSWORD      => bcrypt('admin0'.$i),
                Admin::CREATED_AT    => now(),
                Admin::UPDATED_AT    => now()
            ]);
        }
    }
}
