<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MagazineComment extends Model {
    
    const ID = 'id';
    const MAGAZINE_ID = 'magazine_id';
    const USER_ID = 'user_id';
    const COMMENT = 'comment';
    const IS_APPROVE = 'is_approve';

    protected $guarded = [];

    public function user()  {
        return $this->belongsTo('App\Models\User');
    }

    public function magazine()  {
        return $this->belongsTo('App\Models\Magazine');
    }
}
