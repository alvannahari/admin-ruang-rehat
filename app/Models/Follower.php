<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model {
    
    const ID = 'id';
    const USER_ID = 'user_id';
    const FOLLOWS_ID = 'follows_id';

    protected $guarded = [];
}
