<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Diary extends Model {
    
    const ID = 'id';
    const USER_ID = 'user_id';
    const TITLE = 'title';
    const DIARY = 'diary';
    const IS_PUBLIC = 'is_public';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    public function user()  {
        return $this->belongsTo('App\Models\User');
    }
}
