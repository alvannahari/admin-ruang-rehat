<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MagazineLike extends Model {
    
    const ID = 'id';
    const MAGAZINE_ID = 'magazine_id';
    const USER_ID = 'user_id';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];
}
