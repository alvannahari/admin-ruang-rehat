<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedComment extends Model {
    
    const ID = 'id';
    const FEED_ID = 'feed_id';
    const USER_ID = 'user_id';
    const COMMENT = 'comment';
    const IS_REPORTED = 'is_reported';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    public function scopeActive($query) {
        return $query->where(SELF::IS_REPORTED, '0');
    }

    function delete() {
        $this->reports()->delete();

        return parent::delete();
    }

    public function user()  {
        return $this->belongsTo('App\Models\User');
    }

    public function feed()  {
        return $this->belongsTo('App\Models\Feed');
    }

    function reports() {
        return $this->morphMany('App\Models\Report', 'contentable');
    }
}
