<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model {
    
    const ID = 'id';
    const USER_ID = 'user_id';
    const CONTENT = 'content';
    const IS_ANONYM = 'is_anonymous';
    const IS_REPORTED = 'is_reported';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        $this->comments()->delete();
        $this->reports()->delete();

        return parent::delete();
    }

    public function scopeActive($query) {
        return $query->where(SELF::IS_REPORTED, '0');
    }

    public function user()  {
        return $this->belongsTo('App\Models\User');
    }

    function comments() {
        return $this->hasMany('App\Models\FeedComment');
    }

    function reports() {
        return $this->morphMany('App\Models\Report', 'contentable');
    }
}
