<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model {

    const ID = 'id';
    const USER_ID = 'user_id';
    const CONTENTABLE_ID = 'contentable_id';
    const CONTENTABLE_TYPE = 'contentable_type';
    const ARGUMENT = 'argument';

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];
    
    protected $guarded = [];

    function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function contentable() {
        return $this->morphTo();
    }
}
