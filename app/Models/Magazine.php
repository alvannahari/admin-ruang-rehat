<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model {
    
    const ID = 'id';
    const USER_ID = 'user_id';
    const IS_ADMIN = 'is_admin';
    const TITLE = 'title';
    const CONTENT = 'content';
    const PHOTO = 'photo';
    const IS_APPROVE = 'is_approve';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        $this->comments()->delete();
        $this->userLike()->detach();

        return parent::delete();
    }

    public function getPhotoAttribute() {
        return asset(COVER_MAGAZINE_PATH).'/'.$this->attributes[SELF::PHOTO];
    }

    public function user()  {
        return $this->belongsTo('App\Models\User');
    }

    function like($user_id) {
        $this->userLike()->attach($user_id);
        return $this;
    }

    function unlike($user_id) {
        $this->userLike()->detach($user_id);
        return $this;
    }

    function isLike($user_id) {
        return (boolean) $this->userLike()->where('user_id', $user_id)->first(['users.'.SELF::ID]);
    }

    function comments() {
        return $this->hasMany('App\Models\MagazineComment');
    }

    function userLike() {
        return $this->belongsToMany('App\Models\User', 'magazine_likes', 'magazine_id', 'user_id')->withPivot(SELF::CREATED_AT);
    }
}
