<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {
    
    use HasApiTokens, Notifiable;

    const ID = 'id';
    const EMAIL = 'email';
    const USERNAME = 'username';
    const PASSWORD = 'password';
    const PHONE = 'phone';
    const FULLNAME = 'fullname';
    const PHOTO = 'photo';

    protected $guarded = [];

    protected $hidden = [SELF::PASSWORD];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        $this->feeds()->delete();

        // $this->followers()->delete();
        // $this->follows()->delete();
        $this->followers()->detach();
        $this->follows()->detach();

        $this->diaries()->delete();
        $this->feedComments()->delete();

        // $this->magazineLike()->delete();
        $this->magazineLike()->detach();

        $this->magazines()->delete();
        $this->reports()->delete();

        return parent::delete();
    }

    public function getPhotoAttribute() {
        if (auth()->guard('web')->check() && $this->attributes[SELF::PHOTO] == null) {
            return asset(PHOTO_USER_PATH).'/default.png';
        } else if ($this->attributes[SELF::PHOTO] == null) {
            return $this->attributes[SELF::PHOTO];
        } else {
            return asset(PHOTO_USER_PATH).'/'.$this->attributes[SELF::PHOTO];
        }
    }

    function follow($user_id) {
        $this->follows()->attach($user_id);
        return $this;
    }

    public function unfollow($user_id) {
        $this->follows()->detach($user_id);
        return $this;
    }

    function followers() {
        return $this->belongsToMany(self::class, 'followers', 'follows_id', 'user_id')->withPivot(SELF::CREATED_AT);
    }

    public function follows()  {
        return $this->belongsToMany(self::class, 'followers', 'user_id', 'follows_id')->withPivot(SELF::CREATED_AT);
    }

    public function isFollowing($user_id)  {
        return (boolean) $this->follows()->where('follows_id', $user_id)->first(['users.'.SELF::ID]);
    }

    // public function receivesBroadcastNotificationsOn() {
    //     return 'App.User.'.$this->id;
    // }

    function feeds() {
        return $this->hasMany('App\Models\Feed');
    }

    function magazines() {
        return $this->hasMany('App\Models\Magazine');
    }
    
    function hasMagazine($magazine_id) {
        return (boolean) $this->magazines()->where(SELF::ID, $magazine_id)->first(['magazines.'.SELF::ID]);
    }

    function diaries() {
        return $this->hasMany('App\Models\Diary');
    }

    function feedComments() {
        return $this->hasMany('App\Models\FeedComment');
    }

    function magazineComments() {
        return $this->hasMany('App\Models\MagazineComment');
    }

    function magazineLike() {
        return $this->belongsToMany('App\Models\Magazine', 'magazine_likes', 'user_id', 'magazine_id')->withPivot(SELF::CREATED_AT);
    }

    function reports() {
        return $this->hasMany('App\Models\Report');
    }

    function hasReport($content_id, $type) {
        return (boolean) $this->reports()->where('contentable_id', $content_id)->where('contentable_type', $type)->first(['reports.'.SELF::ID]);
    }

    function isLikeMagazine($magazine_id) {
        return (boolean) $this->magazineLike()->where('magazine_id', $magazine_id)->first(['magazine_likes.'.SELF::ID]);
    }

    function likeMagazine($magazine_id) {
        $this->magazineLike()->attach($magazine_id);
        return $this;
    }

    function unlikeMagazine($magazine_id) {
        $this->magazineLike()->detach($magazine_id);
        return $this;
    }
}
