<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable {
    
    use Notifiable;

    const ID = 'id';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const VERIFIED_AT = 'email_verified_at';
    const REMEMBER_TOKEN = 'remember_token';

    protected $guarded = [];

    protected $hidden = [SELF::PASSWORD, SELF::REMEMBER_TOKEN];

    public function receivesBroadcastNotificationsOn() {
        return 'App.Admin.'.$this->id;
    }
}
