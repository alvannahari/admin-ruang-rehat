<?php 
    define('API_STATUS', 'status');
    define('API_MESSAGE', 'message');
    define('API_DATA', 'data');

    define('UPLOAD_COVER_MAGAZINE', 'assets/images/magazine');
    define('UPLOAD_PHOTO_USER', 'assets/images/user');

    define('PHOTO_USER_PATH', 'assets/images/user');
    define('COVER_MAGAZINE_PATH', 'assets/images/magazine');
?>