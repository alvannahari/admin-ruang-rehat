<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Feed;
use App\Models\Magazine;
use Carbon\Carbon;

class HomeController extends Controller {
    
    public function index() {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $getActivity = Feed::whereBetween(Feed::CREATED_AT, [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->groupBy('date')->select(DB::raw('DATE_FORMAT(created_at, "%d") as date'), DB::raw('count(*) as total'))->get()->toArray();
        $data['activity'] = $this->_graphActivity($getActivity);

        $data['histories'] = Feed::with('user')->orderBy(FEED::CREATED_AT, 'desc')->take(10)->get()->toArray();
        $feed = Feed::select(DB::raw('is_anonymous, count(id) as total'))->groupBy(Feed::IS_ANONYM)->get()->toArray();
        if (!empty($feed)) {
            foreach ($feed as $key => $value) {
                if ($value[Feed::IS_ANONYM] == 0) {
                    $data['feed']['label'][$key] = 'Tidak Anonymous';
                    $data['feed']['total'][$key] = $value['total'];
                } 
                if ($value[Feed::IS_ANONYM] == 1) {
                    $data['feed']['label'][$key] = 'Anonymous';
                    $data['feed']['total'][$key] = $value['total'];
                } 
            }
        } else {
            $data['feed']['label'][0] = 'Status';
            $data['feed']['total'][0] = 1;
        }

        $magazine = Magazine::select(DB::raw('is_approve, count(id) as total'))->groupBy(Magazine::IS_APPROVE)->get()->toArray();
        if (!empty($magazine)) {
            foreach ($magazine as $key => $value) {
                if ($value[Magazine::IS_APPROVE] == 0) {
                    $data['magazine']['label'][$key] = 'Ditangguhkan';
                    $data['magazine']['total'][$key] = $value['total'];
                } 
                if ($value[Magazine::IS_APPROVE] == 1) {
                    $data['magazine']['label'][$key] = 'Disetujui';
                    $data['magazine']['total'][$key] = $value['total'];
                } 
            }
        } else {
            $data['magazine']['label'][0] = 'Status';
            $data['magazine']['total'][0] = 1;
        }
        // dd($data);
        return view('home', compact('data'));
    }

    function notifications() {
        return auth()->user()->unreadNotifications()->get()->toArray();
        // $data = auth()->user()->unreadNotifications()->where('data','like','%"magazine":{"id":11,%')->get();
        // return $data->markAsRead();
    }

    private function _graphActivity(array $activity) {
        $data['value'] = [];

        $data['label'] = [
            'Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'
        ];

        // foreach($nameDay as $index => $name) {
        for ($i=0; $i<7; $i++){
            $temp = false;
            $thisDate = Carbon::now()->startOfWeek()->addDay($i)->format('d');
            foreach($activity as $key => $value) {
                if($value['date'] == $thisDate){
                    $data['value'][$i] = $value['total'];
                    $temp = true;
                    break;
                };
            };
            if(!$temp) {
                $data['value'][$i] = 0;
            }
        };

        return $data;
    }
}
