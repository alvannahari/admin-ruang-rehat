<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Magazine;
use App\Models\MagazineComment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class MagazineController extends Controller {

    const IMAGE_PATH = 'assets/images/magazine';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('magazine.index');
    }

    function getAllMagazine() {
        $data = Magazine::with('user')->get([Magazine::ID, Magazine::USER_ID, Magazine::IS_ADMIN, Magazine::TITLE, Magazine::IS_APPROVE, Magazine::CREATED_AT])->toArray();
        return response(['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            Magazine::TITLE     => 'required|string',
            Magazine::CONTENT   => 'required',
            Magazine::PHOTO     => 'required|image|mimes:png,jpg,jpeg|max:4112',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        $credentials = $request->only([Magazine::TITLE, Magazine::CONTENT]);
        $credentials[Magazine::USER_ID] = 0;
        $create = Magazine::create($credentials);
        $uid = $create->id;

        if ($files = $request->file(Magazine::PHOTO)) { 
            $destinationPath = public_path(SELF::IMAGE_PATH);
            $filename = $uid.'_sampul_magazine.'.$files->getClientOriginalExtension();
            $files->move($destinationPath, $filename);
        }

        $update = Magazine::where('id', $uid)->update([
            Magazine::PHOTO => $filename ?? 'default.jpg'
        ]);

        if ($update) {
            return response(['error' => false]);
        }
        return response(['error' => true, 'message' => 'Terjadi Kesalahan !']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Magazine $magazine) {
        if (!$this->markAsRead($magazine->id)) {
            abort(404);
        }
        $magazine = Magazine::with('user','comments.user','userLike')->withCount('comments','userLike')->find($magazine->id)->toArray();
        // dd($magazine);
        return view('magazine.detail', compact('magazine'));
    }

    function status(Magazine $magazine, $status) {
        $update = $magazine->update([
            Magazine::IS_APPROVE    => $status
        ]);

        if ($update) {
            return redirect()->back();
        }
        return redirect()->back();
    }

    public function update(Request $request, Magazine $magazine) {
        $validator = Validator::make($request->all(), [
            Magazine::TITLE         => 'required|string',
            Magazine::CONTENT       => 'required',
            Magazine::PHOTO         => 'image|mimes:png,jpg,jpeg|max:4112',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
            exit();
        }

        if ($request->hasFile(Magazine::PHOTO) && $files = $request->file(Magazine::PHOTO)) { 
            $destinationPath = public_path(SELF::IMAGE_PATH);
            $filename = $magazine->id.'_sampul_magazine.'.$files->getClientOriginalExtension();
            $files->move($destinationPath, $filename);

            $update = $magazine->update([
                Magazine::TITLE         => $request->title,
                Magazine::CONTENT       => $request->content,
                Magazine::PHOTO         => $filename
            ]);

        } else {
            $update = $magazine->update([
                Magazine::TITLE => $request->title,
                Magazine::CONTENT => $request->content
            ]);
        }

        if ($update) {
            return response(['error' => false]);
        }
        return response(['error' => true, 'message' => 'Terjadi Kesalahan !']);
    }

    function updateComment(MagazineComment $magazineComment, $status) {
        // return response(['id' => $magazineComment->id, 'status' => $status]);
        $update = $magazineComment->update([
            MagazineComment::IS_APPROVE => $status
        ]);
        if ($update) {
            return response(['error' => false]);
        }
        return response(['error' => 'Status komentar ini tidak dapat melakukan perubahan!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Magazine $magazine) {
        if ($magazine->photo != null || $magazine->photo != 'default.png') {
            $filepath = public_path().'/'.SELF::IMAGE_PATH.'/'.$magazine->photo;
            File::delete($filepath);
        }
        if ($magazine->delete()) {
            return response(['error' => false]);
        }
        return response(['error' => 'Konten Magazine Ini Gagal Terhapus!']);
    }

    public function destroyComment(MagazineComment $magazineComment) {
        if ($magazineComment->delete()) {
            return response(['error' => false]);
        }
        return response(['error' => 'Komentar Magazine Ini Gagal Terhapus!']);
    }

    private function markAsRead($idMagazine) {
        auth()->user()->unreadNotifications()->where('data','like','%"magazine":{"id":'.$idMagazine.',%')->get()->markAsRead();
        return true;
    }
}
