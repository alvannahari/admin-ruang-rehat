<?php

namespace App\Http\Controllers\Api\V1\Feed;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Feed;

class GetDetail extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Feed::ID    => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $feed = Feed::with('user','comments.user')->withCount('comments')->find($request->id)->toArray();

        return APIresponse(true, 'Data Berhasil Ditemukan!', $feed);
    }
}
