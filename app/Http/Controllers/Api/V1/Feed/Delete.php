<?php

namespace App\Http\Controllers\Api\V1\Feed;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Feed;
use Illuminate\Support\Facades\Validator;

class Delete extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Feed::ID        => ['required', 'integer'],
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $feed = Feed::find($request->id);
        if (empty($feed)) return APIresponse(false, 'Data Feed Tidak Ditemukan!', null);
        
        if ($feed->delete()) return APIresponse(true, 'Data Feed Berhasil Dihapus!', null);
        
        return APIresponse(false, 'Data Feed Gagal Terhapus!', null);
    }
}
