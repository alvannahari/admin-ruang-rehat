<?php

namespace App\Http\Controllers\Api\V1\Feed;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Follower;
use App\Models\Feed;

class GetAllPostByUser extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Feed::USER_ID       => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $user = User::find($request->user_id);

        $userIds = $user->follows()->pluck(Follower::FOLLOWS_ID)->toArray();
        $userIds[] = $user->id;
        $feed = Feed::active()->with('user')->withCount('comments')->orderBy(Feed::CREATED_AT, 'DESC')->get()->toArray();
        foreach ($feed as $key => $value) {
            if (in_array($value[Feed::USER_ID], $userIds)) {
                $feed[$key]['isFollowed'] = true;
            } else {
                $feed[$key]['isFollowed'] = false;
            }
            if ($user->hasReport($value[Feed::ID], 'feed')) {
                unset($feed[$key]);
            }
        }

        return APIresponse(true, 'Data Berhasil Ditemukan!', $feed);
    }
}
