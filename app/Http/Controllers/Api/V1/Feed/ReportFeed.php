<?php

namespace App\Http\Controllers\Api\V1\Feed;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Feed;
use App\Models\Report;
use Illuminate\Http\Request;

class ReportFeed extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Report::USER_ID                 => 'required|numeric',
            Report::CONTENTABLE_ID          => 'required|numeric',
            Report::ARGUMENT                => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only(Report::USER_ID, Report::CONTENTABLE_ID, Report::ARGUMENT);
        $credentials[Report::CONTENTABLE_TYPE] = 'feed';

        Report::create($credentials);

        return APIresponse(true, 'Laporan Berhasil Dikirimkan!', null);
    }
}
