<?php

namespace App\Http\Controllers\Api\V1\Feed;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\FeedComment;

class LeaveComment extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            FeedComment::USER_ID    => ['required', 'numeric'],
            FeedComment::FEED_ID    => ['required', 'numeric'],
            FeedComment::COMMENT    => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only(FeedComment::USER_ID, FeedComment::FEED_ID, FeedComment::COMMENT);

        $comment = FeedComment::create($credentials);

        return APIresponse(true, 'Komentar Post Berhasil Ditambahkan', $comment);
    }
}
