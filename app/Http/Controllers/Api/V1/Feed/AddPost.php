<?php

namespace App\Http\Controllers\Api\V1\Feed;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Feed;

class AddPost extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Feed::USER_ID   => ['required', 'numeric'],
            Feed::CONTENT   => ['required', 'string', 'max:1000'],
            Feed::IS_ANONYM => ['required', 'numeric'],
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only(Feed::USER_ID, Feed::CONTENT, Feed::IS_ANONYM);

        $post = Feed::create($credentials);

        return APIresponse(true, 'Data Post Baru Berhasil Ditambahkan!', $post);
    }
}
