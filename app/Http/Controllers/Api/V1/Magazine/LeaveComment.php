<?php

namespace App\Http\Controllers\Api\V1\Magazine;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Magazine;
use App\Models\MagazineComment;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin;
use App\Notifications\NewMagazineComment;

class LeaveComment extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            MagazineComment::MAGAZINE_ID    => ['required', 'numeric'],
            MagazineComment::USER_ID        => ['required', 'numeric'],
            MagazineComment::COMMENT        => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only(MagazineComment::MAGAZINE_ID, MagazineComment::USER_ID, MagazineComment::COMMENT);

        $data = MagazineComment::create($credentials);
        $magazine = Magazine::find($credentials[MagazineComment::MAGAZINE_ID], [Magazine::ID, Magazine::TITLE]);

        $admin = Admin::find(1);
        $admin->notify(new NewMagazineComment($request->user(), $data, $magazine));

        return APIresponse(true, 'Anda Telah Berhasil Menambahkan Komentar di Magazine Ini!', null);
    }
}
