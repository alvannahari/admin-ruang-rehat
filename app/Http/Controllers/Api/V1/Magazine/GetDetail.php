<?php

namespace App\Http\Controllers\Api\V1\Magazine;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Magazine;
use App\Models\User;

class GetDetail extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Magazine::ID        => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $magazine = Magazine::with('user', 'comments.user')->withCount('userLike', 'comments')->find($request->id)->toArray();

        if (empty($magazine)) return APIresponse(false, 'Data Magazine Tidak Ditemukan!', null);

        $user = $request->user();

        if ($user->isLikeMagazine($magazine[Magazine::ID])) $magazine['is_liked'] = true;
        else $magazine['is_liked'] = false;
        
        return APIresponse(true, 'Data Magazine Berhasil Ditemukan!', $magazine);
    }
}
