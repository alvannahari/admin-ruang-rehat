<?php

namespace App\Http\Controllers\Api\V1\Magazine;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Magazine;
use App\Models\MagazineLike;

class GiveLike extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(),[
            MagazineLike::USER_ID       => ['required', 'numeric'],
            MagazineLike::MAGAZINE_ID   => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only(MagazineLike::USER_ID, MagazineLike::MAGAZINE_ID);

        $magazine = Magazine::find($credentials[MagazineLike::MAGAZINE_ID]);

        if ($magazine->isLike($credentials[MagazineLike::USER_ID])) {
            $magazine->unlike($credentials[MagazineLike::USER_ID]);
            return APIresponse(true, 'Anda berhenti Menyukai Magazine Ini!', null);
        }

        $magazine->like($credentials[MagazineLike::USER_ID]);
        return APIresponse(true, 'Anda Berhasil Menyukai Magazine Ini!', null);
    }
}
