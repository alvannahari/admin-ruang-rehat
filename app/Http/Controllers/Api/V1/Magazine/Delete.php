<?php

namespace App\Http\Controllers\Api\V1\Magazine;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Magazine;

class Delete extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Magazine::ID        => ['required', 'integer'],
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $user = $request->user();
        if (!$user->hasMagazine($request->id)) {
            return APIresponse(false, 'Anda Tidak Mempunyai Data Magazine Ini!', null);
        }

        $magazine = Magazine::find($request->id);

        if ($magazine->delete()) {
            return APIresponse(true, 'Data Magazine Berhasil Dihapus!', null);
        }
        return APIresponse(false, 'Data Magazine Gagal Terhapus!', null);
    }
}
