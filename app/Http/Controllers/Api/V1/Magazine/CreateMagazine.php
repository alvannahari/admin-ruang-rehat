<?php

namespace App\Http\Controllers\Api\V1\Magazine;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Magazine;
use App\Models\Admin;
use App\Notifications\NewMagazine;

class CreateMagazine extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Magazine::USER_ID   => ['required', 'numeric'], 
            Magazine::TITLE     => ['required', 'string', 'max:500'],
            Magazine::CONTENT   => ['required', 'string', 'max:5000'],
            Magazine::PHOTO     => ['image','mimes:jpeg,png,jpg,gif','max:4096']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        // return APIresponse(true, 'daada', $request->user());

        $credentials = $request->only(Magazine::USER_ID, Magazine::TITLE, Magazine::CONTENT);
        $credentials[Magazine::IS_ADMIN]    = '0';
        $credentials[Magazine::IS_APPROVE]  = '0';

        $magazine = Magazine::create($credentials);

        if ($request->hasfile(Magazine::PHOTO)) {
            $image = $request->file(Magazine::PHOTO);
            $destinationPath = public_path(UPLOAD_COVER_MAGAZINE);
            $filename = $magazine->id.'_sampul_magazine.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $filename);
            $magazine->update([
                Magazine::PHOTO     => $filename
            ]);
        }

        $admin = Admin::find(1);
        $admin->notify(new NewMagazine($request->user(), $magazine));

        return APIresponse(true, 'Magazine Baru Berhasil Ditambahkan!', null);
    }
}
