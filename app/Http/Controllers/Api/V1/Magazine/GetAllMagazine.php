<?php

namespace App\Http\Controllers\Api\V1\Magazine;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Magazine;
use App\Models\User;

class GetAllMagazine extends Controller {
    
    function __invoke(Request $request) {
        // $data = Magazine::where(Magazine::IS_APPROVE, "1")->with('user')->withCount(['comments','userLike'])->get();
        // $data = Magazine::with('user')->withCount(['comments','userLike'])->select([Magazine::ID, Magazine::USER_ID, Magazine::IS_ADMIN, Magazine::TITLE, Magazine::PHOTO, Magazine::IS_APPROVE, Magazine::CREATED_AT, Magazine::UPDATED_AT])->get()->toArray();
        $user = $request->user();
        $data = Magazine::with('user')->withCount(['comments','userLike'])->get()->toArray();
        foreach ($data as $key => $value) {
            if ($value[Magazine::USER_ID] != $user->id && $value[Magazine::IS_APPROVE] == 0) {
                unset($data[$key]);
            } else {
                if ($user->isLikeMagazine($value[Magazine::ID])) $data[$key]['is_liked'] = true;
                else $data[$key]['is_liked'] = false;
            }
        }
        $data = array_values($data);
        return APIresponse(true, 'Data Magazine Berhasil Ditemukan!', $data);
    }
}
