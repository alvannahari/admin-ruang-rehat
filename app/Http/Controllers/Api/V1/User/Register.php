<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class Register extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::EMAIL         => ['required', 'string', 'email', 'max:255', 'unique:users'],
            User::USERNAME      => ['required', 'string', 'max:255', 'unique:users'],
            User::PASSWORD      => ['required', 'string', 'max:255'],
            User::PHONE         => ['required', 'max:255', 'unique:users'],
            User::FULLNAME      => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only([
            User::EMAIL,
            User::USERNAME,
            User::PASSWORD,
            User::PHONE,
            User::FULLNAME
        ]);

        $credentials[User::PASSWORD] = bcrypt($credentials[User::PASSWORD]);
        $data = User::create($credentials);
        $data['token'] =  $data->createToken('register')-> accessToken; 

        return APIresponse(true, 'Registrasi Akun Berhasil!', $data);
    }
}
