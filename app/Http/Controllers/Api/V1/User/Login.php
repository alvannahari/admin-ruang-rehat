<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Login extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::USERNAME      => ['required', 'string', 'max:255'],
            User::PASSWORD      => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only(User::USERNAME, User::PASSWORD);

        if(Auth::guard('user')->attempt($credentials)) {
            $user = Auth::guard('user')->user();
            $user['token'] =  $user->createToken('login')->accessToken;
            return APIresponse(true, 'Login Berhasil!', $user);
        }
        else{ 
            return APIresponse(false, 'Username Atau Password Belum Terdaftar!', null, 202);
        } 
    }
}
