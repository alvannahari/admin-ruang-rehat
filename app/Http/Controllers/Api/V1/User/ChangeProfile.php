<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class ChangeProfile extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            User::ID            => ['required', 'numeric'],
            User::EMAIL         => ['required', 'string', 'email', 'max:255', 'unique:users,'.User::EMAIL.','.$request->id],
            User::USERNAME      => ['required', 'string', 'alpha_dash', 'max:100'],
            User::PASSWORD      => ['required', 'string', 'max:255'],
            User::PHONE         => ['required', 'numeric', 'unique:users,'.User::PHONE.','.$request->id],
            User::FULLNAME      => ['required', 'string', 'max:255'],
            User::PHOTO         => ['image','mimes:png,jpg,jpeg,webp','max:8224'],
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only([
            User::EMAIL,
            User::USERNAME,
            User::PASSWORD,
            User::PHONE,
            User::FULLNAME
        ]);
        $credentials[User::PASSWORD] = bcrypt($credentials[User::PASSWORD]);

        if ($request->hasfile(User::PHOTO)) {
            $image = $request->file(User::PHOTO);
            $destinationPath = public_path(UPLOAD_PHOTO_USER);
            $filename = 'user_'.$request->id.'_profile.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $filename);
            $credentials[User::PHOTO] = $filename;
        }

        if (User::where(User::ID, $request->id)->update($credentials)) {
            $credentials[User::ID] = $request->id;
            return APIresponse(true, 'Update Profil User Berhasil!', $credentials);
        }
        
        return APIresponse(false, 'Update Profil User Gagal!', null, 202);
    }
}
