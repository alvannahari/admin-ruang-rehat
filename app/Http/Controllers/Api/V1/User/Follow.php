<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Follower;

class Follow extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Follower::USER_ID       => ['required', 'numeric'],
            Follower::FOLLOWS_ID    => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only([Follower::USER_ID, Follower::FOLLOWS_ID]);

        if ($credentials[Follower::USER_ID] == $credentials[Follower::FOLLOWS_ID]) 
            return APIresponse(false, 'Anda Tidak Bisa Mengikuti Diri Sendiri', null, 202);

        $user = User::find($credentials[Follower::USER_ID]);

        if ($user->isFollowing($credentials[Follower::FOLLOWS_ID])) 
            return APIresponse(false, 'Anda Sudah Mengikuti Pengguna Ini Sebelumnya!', null, 400);

        $user->follow($credentials[Follower::FOLLOWS_ID]);
        return APIresponse(true, 'Sekarang Anda Telah Mengikuti Pengguna Ini', null);
    }
}
