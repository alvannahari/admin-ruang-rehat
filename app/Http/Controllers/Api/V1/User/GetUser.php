<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Follower;

class GetUser extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Follower::USER_ID       => ['required', 'numeric'],
            Follower::FOLLOWS_ID    => ['required', 'numeric', 'different:user_id']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only([Follower::USER_ID, Follower::FOLLOWS_ID]);

        $data = User::with(['feeds', 'magazines', 'diaries'])->withCount(['followers', 'follows'])->find($credentials[Follower::FOLLOWS_ID])->toArray();

        if (empty($data)) return APIresponse(false, 'Data User Tidak Ditemukan!', null, 404);
        
        $data['is_followed'] = User::find($credentials[Follower::USER_ID])->isFollowing($credentials[Follower::FOLLOWS_ID]);
        
        return APIresponse(true, 'Data User Berhasil Ditemukan!', $data);
    }
}
