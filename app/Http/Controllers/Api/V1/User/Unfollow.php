<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Follower;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class Unfollow extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Follower::USER_ID       => ['required', 'numeric'],
            Follower::FOLLOWS_ID    => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only([Follower::USER_ID, Follower::FOLLOWS_ID]);

        if ($credentials[Follower::USER_ID] == $credentials[Follower::FOLLOWS_ID]) 
            return APIresponse(false, 'Anda Tidak Bisa Mengikuti Diri Sendiri', null, 202);

        $user = User::find($credentials[Follower::USER_ID]);

        if (!$user->isFollowing($credentials[Follower::FOLLOWS_ID])) 
            return APIresponse(false, 'Anda Belum Mengikuti Pengguna Ini!', null, 400);
        
        $user->unfollow($credentials[Follower::FOLLOWS_ID]);
        return APIresponse(true, 'Sekarang Anda Berhenti Mengikuti Pengguna Ini', null);
    }
}
