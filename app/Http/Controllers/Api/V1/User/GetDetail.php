<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class GetDetail extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(),[
            User::ID    => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $data = User::with(['feeds', 'magazines', 'diaries', 'follows'])->withCount(['followers', 'follows'])->find($request->id)->toArray();

        if (!empty($data)) {
            return APIresponse(true, 'Data User Berhasil Ditemukan!', $data);
        }

        return APIresponse(false, 'Data User Gagal Ditemukan!', null, 404);
    }
}
