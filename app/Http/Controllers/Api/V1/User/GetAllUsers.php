<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class GetAllUsers extends Controller {

    function __invoke(Request $request) {

        $data = User::get()->toArray();
        
        return APIresponse(true, 'Hasil Pencarian Data User!', $data);
    }
}
