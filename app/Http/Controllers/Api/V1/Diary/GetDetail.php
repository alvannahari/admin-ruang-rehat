<?php

namespace App\Http\Controllers\Api\V1\Diary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Diary;
use Illuminate\Support\Facades\Validator;

class GetDetail extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Diary::ID        => ['required', 'integer'],
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $diary = Diary::with('user')->find($request->id);
        if (empty($diary)) return APIresponse(false, 'Data Diary Tidak Ditemukan!', null);

        return APIresponse(true, 'Data Diary Ditemukan!', $diary->toArray());
    }
}
