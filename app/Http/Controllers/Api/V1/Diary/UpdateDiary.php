<?php

namespace App\Http\Controllers\Api\V1\Diary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Diary;

class UpdateDiary extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Diary::ID           => ['required', 'numeric'],
            Diary::TITLE        => ['required', 'string', 'max:255'],
            Diary::DIARY        => ['required', 'string', 'max:300'],
            Diary::IS_PUBLIC    => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->only(Diary::TITLE, Diary::DIARY, Diary::IS_PUBLIC);

        $data = Diary::where(Diary::ID, $request->id)->update($credentials);

        if ($data) {
            return APIresponse(true, 'Data Diary Berhasil Diperbarui!', null);
        }

        return APIresponse(false, 'data Diary Gagal Diperbarui', null);
    }
}
