<?php

namespace App\Http\Controllers\Api\V1\Diary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Diary;

class GetDiaryByUser extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Diary::USER_ID      => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $diary = Diary::where(Diary::USER_ID, $request->user_id)->with('user')->get()->toArray();

        // foreach ($diary as $key => $value) {
        //     $diary[$key]['user']['photo'] = asset(PHOTO_USER_PATH).'/'.$value['user']['photo'];
        // }

        return APIresponse(true, 'Data Diari Berhasil Ditemukan!', $diary);
    }
}
