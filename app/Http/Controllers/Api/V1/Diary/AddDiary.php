<?php

namespace App\Http\Controllers\Api\V1\Diary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Diary;

class AddDiary extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            Diary::USER_ID      => ['required', 'numeric'],
            Diary::TITLE        => ['required', 'string', 'max:500'],
            Diary::DIARY        => ['required', 'string', 'max:5000'],
            Diary::IS_PUBLIC    => ['required', 'numeric'],
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $credentials = $request->toArray();

        Diary::create($credentials);

        return APIresponse(true, 'Data Diary Berhasil Ditambahkan!', null);
    }
}
