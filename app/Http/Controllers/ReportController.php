<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Report;
use App\Models\Feed;
use App\Models\FeedComment;

class ReportController extends Controller {
    
    function index() {
        $data = Report::with('user', 'contentable')->get()->toArray();

        dd($data);
    }

    function reportFeed(Feed $feed) {
        $status = $feed->is_reported;
        if ($status == '0') $status = '1';
        else $status = '0';
        
        $update = $feed->update([
            Feed::IS_REPORTED => $status
        ]);

        if ($update) {
            return redirect()->back();
        }
        return redirect()->back();
    }

    function reportFeedComment(FeedComment $feedComment) {
        $status = $feedComment->is_reported;
        if ($status == '0') $status = '1';
        else $status = '0';
        
        $update = $feedComment->update([
            FeedComment::IS_REPORTED => $status
        ]);

        if ($update) {
            return redirect()->back();
        }
        return redirect()->back();
    }
}
