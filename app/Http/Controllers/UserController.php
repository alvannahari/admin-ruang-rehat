<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\Magazine;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UserController extends Controller {

    const URL_PATH = 'assets/images/users';
    
    function index() {
        return view('user.index');
    }

    function getAllUser() {
        $data = User::withCount('magazines','diaries','feeds')->get()->toArray();
        // dd($data);
        return response(['data' => $data]);
    }

    function show(User $user) {
        $user_id = $user->id;
        $user = User::with('feeds.comments','diaries','reports.contentable','followers','follows')->withCount('feeds','magazines','diaries','reports','followers','follows')->find($user_id)->toArray();

        $magazines = Magazine::with(['comments', 'userLike'])->where(Magazine::USER_ID, $user_id)->select([Magazine::ID, Magazine::USER_ID, Magazine::IS_ADMIN, Magazine::TITLE, Magazine::IS_APPROVE, Magazine::CREATED_AT, Magazine::UPDATED_AT])->get()->toArray();
        foreach ($magazines as $key => $value) {
            $magazines[$key]['comments_count'] = count($value['comments']);
            $magazines[$key]['user_like_count'] = count($value['user_like']);
            unset($magazines[$key]['comments']);
            unset($magazines[$key]['user_like']);
        };

        // dd($user);
        return view('user.detail', compact('user', 'magazines'));
    }

    function destroy(User $user) {
        if ($user->photo != null) {
            $filepath = public_path().'/'.SELF::URL_PATH.'/'.$user->photo;
            File::delete($filepath);
        }
        if ($user->delete()) {
            return response(['error' => false]);
        }
        return response(['error' => 'Data user gagal terhapus!']);
    }
}
