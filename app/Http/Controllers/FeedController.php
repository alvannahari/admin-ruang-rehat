<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feed;
use App\Models\FeedComment;
use App\Models\User;

class FeedController extends Controller {
    
    function index() {
        return view('feed.index');
    }

    function getAllfeeds() {
        // $feeds = Feed::with('user')->withCount('reports')->get()->toArray();
        $feeds = Feed::withCount('reports')->get()->toArray();
        // foreach ($feeds as $key => $value) {
        //     $feeds[$key]['feed'] = $value['user'][User::FULLNAME].' - '.$value[Feed::CONTENT];
        //     unset($feeds[$key]['user']);
        // };
        return response(['data' => $feeds]);
    }

    function show(Feed $feed) {
        $feed = Feed::with('user', 'reports.user','comments.user','comments.reports.user')->withCount('comments', 'reports')->find($feed->id)->toArray();
        foreach ($feed['comments'] as $key => $value) {
            $feed['comments'][$key]['count_reports'] = count($value['reports']);
        }
        // dd($feed);
        return view('feed.detail', compact('feed'));
    }

    function destroy(Feed $feed) {
        if ($feed->delete()) {
            return response(['error' => false]);
        }
        return response(['error' => 'Feed atau Post Gagal Dihapus !']);
    }

    function destroyComment(FeedComment $feedComment) {
        if ($feedComment->delete()) {
            return redirect()->back();
        }
        return redirect()->back();
    }
}
