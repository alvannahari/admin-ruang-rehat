<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Diary;

class DiaryController extends Controller {
    
    function destroy(Diary $diary) {
        if ($diary->delete()) {
            return response(['error' => false]);
        }
        return response(['error' => 'Terjadi Kesalahan, Diary ini gagal terhapus!!!']);
    }
}
