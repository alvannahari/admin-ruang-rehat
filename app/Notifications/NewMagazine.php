<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\User;
use App\Models\Magazine;

class NewMagazine extends Notification {

    use Queueable;

    protected $user;
    protected $magazine;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Magazine $magazine) {
        $this->user = $user->toArray();
        $this->magazine = $magazine->toArray();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['database', 'broadcast'];
    }

    public function toDatabase($notifiable)
    {
        return [
            User::FULLNAME  => $this->user[User::FULLNAME],
            User::PHOTO     => $this->user[User::PHOTO],
            'magazine'  => [
                Magazine::ID            => $this->magazine[Magazine::ID],
                Magazine::TITLE         => $this->magazine[Magazine::TITLE],
            ]
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'created_at' => now(),
            'data' => [
                User::FULLNAME  => $this->user[User::FULLNAME],
                User::PHOTO     => $this->user[User::PHOTO],
                'magazine'  => [
                    Magazine::ID            => $this->magazine[Magazine::ID],
                    Magazine::TITLE         => $this->magazine[Magazine::TITLE],
                ]
            ],
        ];
    }
}
