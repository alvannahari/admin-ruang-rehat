@extends('template.layout')

@section('title')
    Detail User
@endsection

@section('content')
<div class="card">
    <span class="card-title p-3"> 
        <div class="row">
            <div class="col-6">
                <span style="font-size: 18px;" >Data User </span>
            </div>
            <div class="col-6">
                <button class="btn btn-sm btn-danger" style="float: right" data-toggle="modal" data-target="#modal-delete"> <i class="mdi mdi-delete"></i> Hapus User</button>
                {{-- <button class="btn btn-sm btn-warning m-r-15" style="float: right" data-toggle="modal" data-target="#modal-update-group"> <i class="mdi mdi-update"></i> Ubah</button> --}}
            </div>
        </div>
    </span>
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-3 col-lg-2 pr-sm-0 mb-2" style="text-align: center">
                <img class="rounded-circle" src="{{ $user['photo'] }}" alt="" srcset="" width="120px" height="120px">
            </div>
            <div class="col-12 col-sm-9 col-lg-10">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label>Nama Lengkap</label>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $user['fullname'] ?? 'Admin Rumah Rehat' }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label>Username</label>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $user['username'] ?? 'Username Admin' }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label>Email</label>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $user['email'] ?? 'Email Admin' }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label>No. Telepon</label>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $user['phone'] ?? '+628563194806' }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label>Dibuat Tanggal</label>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $user['created_at'] ?? now() }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label>Diperbarui Tanggal</label>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $user['updated_at'] ?? now() }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label>Pengikut</label>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $user['followers_count'] ?? 0 }} <label class="label label-rounded label-info ml-2 mb-0" style="font-size: 80%;cursor: pointer;" data-toggle="modal" data-target="#modal-followers"><i class="mdi mdi-eye-outline"></i> Lihat</label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-4">
                                    <label>Mengikuti</label>
                                </div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $user['follows_count'] ?? 0 }} <label class="label label-rounded label-info ml-2 mb-0" style="font-size: 80%;cursor: pointer;" data-toggle="modal" data-target="#modal-following"><i class="mdi mdi-eye-outline"></i> Lihat</label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <p class="mb-1"><b>{{ $user['fullname'] ?? 'Admin Rumah Rehat' }}</b></p>
                <p>{{ $user['username'] ?? 'Username Admin' }}</p>
                <p>{{ $user['email'] ?? 'Email Admin' }}</p>
                <p>{{ $user['phone'] ?? 'Phone Number Admin' }}</p> --}}
                {{-- <div class="row">
                    <div class="col-8 col-lg-10">
                    </div>
                    <div class="col-4 col-lg-2" style="text-align: center">
                        <button class="btn btn-sm btn-warning my-2" style="width: 100%"><i class="mdi mdi-update"></i> Ubah Profil</button>
                        <button class="btn btn-sm btn-danger my-2" style="width: 100%"><i class="mdi mdi-delete"></i> Hapus</button>
                    </div>
                </div> --}}
                {{-- <div class="row" style="text-align: center">
                        <div class="col-6 col-lg-3 p-1">
                            <a href="#" data-toggle="modal" data-target="#modal-followers"><p class="border-left border-right"><b class="mr-2">{{ $user['followers_count'] }}</b> Followers</p></a>
                        </div>
                        <div class="col-6 col-lg-3 p-1">
                            <a href="#" data-toggle="modal" data-target="#modal-following"><p class="border-right"><b class="mr-2">{{ $user['follows_count'] }}</b> Following</p></a>
                        </div>
                        <div class="col-6 col-lg-3 p-1">
                            <p class="border-left border-right" title="Tanggal Dibuat" style="color: blue"><i class="mdi mdi-timer"></i><font style="font-size: 13px"> {{$user['created_at'] }}</font></p>
                        </div>
                        <div class="col-6 col-lg-3 p-1">
                            <p class="border-right" title="Terakhir Diperbarui" style="color: blue"><i class="mdi mdi-update"></i><font style="font-size: 13px"> {{$user['updated_at'] }}</font></p>
                        </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>
    
<div class="card">
    <div class="card-body">
        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="post-tab" data-toggle="tab" href="#post" role="tab" aria-controls="post" aria-selected="true">Post <label class="label label-rounded label-custom ml-1">{{ $user['feeds_count'] ?? 0 }}</label></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="magazine-tab" data-toggle="tab" href="#magazine" role="tab" aria-controls="magazine" aria-selected="false">Magazine <label class="label label-rounded label-custom ml-1">{{ $user['magazines_count'] ?? 0 }}</label></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="diary-tab" data-toggle="tab" href="#diary" role="tab" aria-controls="diary" aria-selected="false">Diary <label class="label label-rounded label-custom ml-1">{{ $user['diaries_count'] ?? 0 }}</label></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="report-tab" data-toggle="tab" href="#report" role="tab" aria-controls="report" aria-selected="false">Report <label class="label label-rounded label-custom ml-1">{{ $user['reports_count'] ?? 0 }}</label></a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="post" role="tabpanel" aria-labelledby="post-tab">
                <table class=" table table-hover table-sm" width="100%" >
                    <thead>
                        <tr>
                            <th style="border-top: none;"><center>NO.</center></th>
                            <th style="border-top: none;">POST</th>
                            <th style="border-top: none;">ANONYMOUS</th>
                            <th style="border-top: none;"><center>KOMENTAR</center></th>
                            <th style="border-top: none;min-width:100px"><center>TANGGAL</center></th>
                            <th style="border-top: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($user['feeds'] as $feed)
                            <tr>
                                <td><center>{{ $loop->iteration }}</center></td>
                                <td><span class="my-2">{{ $feed['content'] }}</span></td>
                                <td><center>
                                    @if ($feed['is_anonymous'] == 0)
                                        <label class="label label-rounded label-warning">Tidak</label>
                                    @else
                                        <label class="label label-rounded label-info">Ya</label>
                                    @endif
                                    </center>
                                </td>
                                <td><center>{{ count($feed['comments']) }}</center></td>
                                <td><center>{{ $feed['created_at'] }}</center></td>
                                <td>
                                    <a href="{{ url('feed').'/'.$feed['id'] }}" type="button" class="btn btn-sm btn-primary m-l-5"><i class="mdi mdi-magnify"></i> Detail</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5"><center>User Belum Pernah Membuat Post atau Feed Ini</center></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="magazine" role="tabpanel" aria-labelledby="magazine-tab">
                <table class="table table-hover table-sm" width="100%" >
                    <thead>
                        <tr>
                            <th style="border-top: none;"><center>NO.</center></th>
                            <th style="border-top: none;">JUDUL</th>
                            <th style="border-top: none;"><center>APPROVE</center></th>
                            <th style="border-top: none;"><center>SUKA</center></th>
                            <th style="border-top: none;"><center>KOMENTAR</center></th>
                            <th style="border-top: none;"><center>TANGGAL</center></th>
                            <th style="border-top: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($magazines as $magazine)
                            <tr>
                                <td><center>{{ $loop->iteration }}</center></td>
                                <td><span class="my-2">{{ $magazine['title'] }}</span></td>
                                <td><center>
                                    @if ($magazine['is_approve'] == 0)
                                        <label class="label label-rounded label-warning">Ditangguhkan</label>
                                    @else
                                        <label class="label label-rounded label-info">Disetujui</label>
                                    @endif
                                    </center>
                                </td>
                                <td><center>55</center></td>
                                <td><center>20</center></td>
                                <td><center>{{ $magazine['created_at'] }}</center></td>
                                <td>
                                    <a href="{{ url('magazine').'/'.$magazine['id'] }}" type="button" class="btn btn-sm btn-primary m-l-5"><i class="mdi mdi-magnify"></i> Detail</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7"><center>User Belum Pernah Membuat Konten Magazine</center></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="diary" role="tabpanel" aria-labelledby="diary-tab">
                <table class=" table table-hover table-sm" width="100%" >
                    <thead>
                        <tr>
                            <th style="border-top: none;"><center>NO.</center></th>
                            <th style="border-top: none;">JUDUL</th>
                            <th style="border-top: none;"><center>PUBLIK</center></th>
                            <th style="border-top: none;"><center>DIBUAT</center></th>
                            <th style="border-top: none;"><center>DIPERBARUI</center></th>
                            <th style="border-top: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($user['diaries'] as $diary)
                            <tr>
                                <td><center>{{ $loop->iteration }}</center></td>
                                <td><span class="my-2">{{ $diary['title'] }}</span></td>
                                <td><center>
                                    @if ($diary['is_public'] == 0)
                                        <label class="label label-rounded label-warning">Tidak</label>
                                    @else
                                        <label class="label label-rounded label-info">Ya</label>
                                    @endif
                                    </center>
                                </td>
                                <td><center>{{ $diary['created_at'] }}</center></td>
                                <td><center>{{ $diary['updated_at'] }}</center></td>
                                <td><center>
                                    <button class="btn btn-sm btn-primary" onclick="viewDiary({{ $loop->iteration }})"><i class="mdi mdi-eye-outline"></i> Lihat</button>
                                    <button class="btn btn-sm btn-danger" onclick="deleteDiary({{ $diary['id'] }})"><i class="mdi mdi-delete"></i> Hapus</button>
                                    </center>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5"><center>User Belum Pernah Membuat Konten Diary</center></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="report" role="tabpanel" aria-labelledby="report-tab">
                <table class=" table table-hover table-sm" width="100%" >
                    <thead>
                        <tr>
                            <th style="border-top: none;"><center>NO.</center></th>
                            <th style="border-top: none;">ALASAN</th>
                            <th style="border-top: none;"><center>JENIS</center></th>
                            <th style="border-top: none;">KONTEN</th>
                            <th style="border-top: none;min-width:100px"><center>TANGGAL</center></th>
                            <th style="border-top: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($user['reports'] as $report)
                            <tr>
                                <td><center>{{ $loop->iteration }}</center></td>
                                <td><span class="my-2">{{ $report['argument'] }}</span></td>
                                <td><center><label class="label label-rounded label-info">{{ $report['contentable_type'] }}</label></center>
                                </td>
                                <td>{{ $report['contentable_type'] == 'feed' ? $report['contentable']['content'] : $report['contentable']['comment'] }}</td>
                                <td><center>{{ $report['created_at'] }}</center></td>
                                <td>
                                    <a href="{{ $report['contentable_type'] == 'feed' ? url('feed').'/'.$report['contentable']['id'] : url('feed').'/'.$report['contentable']['feed_id'] }}" type="button" class="btn btn-sm btn-primary m-l-5"><i class="mdi mdi-magnify"></i> Detail</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><center>User Belum Pernah Membuat Laporan Konten</center></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    
<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin Ingin Menghapus Data User Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-followers">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Daftar Pengikut Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm" width="100%">
                    <thead class="thead-light">
                        <tr>
                            <th><center>No.</center></th>
                            <th>Nama Lengkap</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th><center>Tanggal Mengikuti</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($user['followers'] as $follower)
                            <tr>
                                <td><center>{{ $loop->iteration }}</center></td>
                                <td>{{ $follower['fullname'] }}</td>
                                <td>{{ $follower['username'] }}</td>
                                <td>{{ $follower['email'] }}</td>
                                <td><center>{{ date('Y-m-d H:i:s', strtotime($follower['pivot']['created_at'])) }}</center></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5"><center>Pengguna ini belum mempunyai pengikut</center></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-following">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Daftar Mengikuti Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm" width="100%">
                    <thead class="thead-light">
                        <tr>
                            <th><center>No.</center></th>
                            <th>Nama Lengkap</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th><center>Tanggal Mengikuti</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($user['follows'] as $follower)
                            <tr>
                                <td><center>{{ $loop->iteration }}</center></td>
                                <td>{{ $follower['fullname'] }}</td>
                                <td>{{ $follower['username'] }}</td>
                                <td>{{ $follower['email'] }}</td>
                                <td><center>{{ date('Y-m-d H:i:s', strtotime($follower['pivot']['created_at'])) }}</center></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5"><center>Pengguna Belum Pernah Mengikuti Siapapun</center></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-view-diary">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">konten Diari Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center><h4 class="mb-0" id="view-title-diary" style="font-weight: 500;">Judul Diary 1</h4></center>
                <row>
                    <div class="col-12" style="text-align: center">
                        <span class="ml-1" title="Tanggal Dibuat"><i class="mdi mdi-timer"></i> : <font style="font-size: 11px" id="view-created-diary">2020-08-20 15:21:13</font></span>
                        <span class="ml-1" title="Terakhir Diperbarui">| <i class="mdi mdi-update"></i> : <font style="font-size: 11px" id="view-updated-diary">2020-08-20 15:21:13</font></span>
                    </div>
                </row>
                <label class="mt-3" id="view-content-diary" style="white-space: pre-line">
                    konten pembuka untuk diary 1
                </label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-diary">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-delete-diary">
                <p>Apakah Anda Yakin Ingin Menghapus Data Diary Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete-diary">Hapus</button>
            </div>
        </div>
    </div>
</div>

<script>
    let contentDiary = {!! json_encode($user['diaries']) !!};

    $('#btn-delete').click(function() {
        let btn = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('user').'/'.$user['id'].'/delete' }}",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend:function () {
                btn.html('Menghapus...');
                btn.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    // location.reload();
                    history.back();
                } else {
                    alert(response.error);
                    btn.html('Hapus');
                    btn.prop('disabled', false);
                }
            }
        });
    });

    function viewDiary(diaryId) {
        $('#view-title-diary').html(contentDiary[diaryId-1]['title']);
        $('#view-created-diary').html(contentDiary[diaryId-1]['created_at']);
        $('#view-updated-diary').html(contentDiary[diaryId-1]['updated_at']);
        $('#view-content-diary').html(contentDiary[diaryId-1]['diary']);
        $('#modal-view-diary').modal('show');
    }

    function deleteDiary(diaryId) {
        $('#input-delete-diary').val(diaryId);
        $('#modal-delete-diary').modal('show');
    };

    $('#btn-delete-diary').click(function() {
        let btn = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('diary') }}/"+$('#input-delete-diary').val()+"/delete",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend:function () {
                btn.html('Menghapus ...');
                btn.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    btn.html('Hapus');
                    btn.prop('disabled', false);
                }
            }
        });
    });
</script>
@endsection