@extends('template/layout')

@section('title', 'Daftar Pengguna')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-user" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th></th>
                                <th>NAMA LENGKAP</th>
                                <th>USERNAME</th>
                                <th><center>POST</center></th>
                                <th><center>MAGAZINE</center></th>
                                <th><center>DIARY</center></th>
                                <th><center>DIBUAT</center></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- <tr>
                                <td style="width: 5px;text-align: center">2</td>
                                <td>Kategori Kedua</td>
                                <td>Deskripsi untuk Kategori Kedua</td>
                                <td style="text-align: center;width: 170px">21-04-2020 07:15:43</td>
                                <td style="text-align: center;width: 170px">25-04-2020 14:01:21</td>
                                <td style="width: 165px">
                                    <a href="#" class="btn btn-success btn-sm" data-placement="top" data-original-title="Detail"><i class="mdi mdi-magnify"></i> Detail</a>
                                    <button class="btn btn-sm btn-danger m-l-5" onclick="deleteGroup()" ><i class="mdi mdi-delete"></i> Delete</button>
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <input type="hidden" id="input-delete"> --}}
                <p>Apakah Anda Yakin Ingin Menghapus Data User Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
            </div>
        </div>
    </div>
</div>

<script>
    var idUser = 1;

    $(document).ready(function () {
        // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
        let t = $('#table-user').DataTable( {
            "processing": true,
            // "serverSide": true,
            "ajax": "{{ url('/getAllUser') }}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "null", render: function ( data, type, row ) {
                        return `<img class="rounded-circle" src="`+row.photo+`" width="45px" height="45px">`;
                    },"bSortable": false 
                },
                { "data": "fullname"},
                { "data": "username"},
                { "data": "feeds_count", "sClass": "text-center"},
                { "data": "magazines_count", "sClass": "text-center"},
                { "data": "diaries_count", "sClass": "text-center"},
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", render: function ( data, type, row ) {
                        return `
                            <a href="{{ url('user') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i> Detail</a>
                            <button class="btn btn-sm btn-danger m-l-5" onclick="deleteUser(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                            `;
                    },"bSortable": false
                }
            ],
            "createdRow": function (row, data, index) {
                $('td', row).eq(1).addClass('px-0');
                $('td', row).eq(8).css('width', '150px');
            },
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function deleteUser(id) {
        idUser = id;
        $('#modal-delete').modal('show');
    }

    $('#btn-delete').click(function() {
        let btn = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('user') }}/"+idUser+"/delete",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend:function () {
                btn.html('Menghapus ...');
                btn.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    btn.html('Hapus');
                    btn.prop('disabled', false);
                }
            }
        });
    });

</script>

@endsection