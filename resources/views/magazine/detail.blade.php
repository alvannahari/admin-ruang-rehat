@extends('template.layout')

@section('title')
    Detail Magazine
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="card" style="min-height: 310px;height: fit-content;">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-3">
                        <span style="font-size: 18px;" >Konten</span>
                    </div>
                    <div class="col-9">
                        <button class="btn btn-sm btn-danger" style="float: right" data-toggle="modal" data-target="#modal-delete-magazine"> <i class="mdi mdi-delete"></i> Delete</button>
                        <button class="btn btn-sm btn-warning m-r-15" style="float: right" data-toggle="modal" data-target="#modal-update-magazine"> <i class="mdi mdi-table-edit"></i> Ubah</button>
                        <button class="btn btn-sm btn-primary m-r-15" style="float: right" data-toggle="modal" data-target="#modal-status-magazine"> <i class="mdi mdi-update"></i> {{ $magazine['is_approve'] == 0 ? 'Setujui' : 'Tangguhkan' }}</button>
                    </div>
                </div>
            </span>
            <div class="card-body" id="content-height">
                <div class="jumbotron jumbotron-fluid mb-1 pb-1" style="background: url('{{ $magazine['photo'] }}');background-size: cover;position: relative;padding-top:7rem">
                    <div class="overlay"></div>
                    <div class="inner" >
                        <h3 class="ml-2" title="Judul Magazine" style="color: white;text-transform:capitalize;">{{ $magazine['title'] }}</h3>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-1 col-2 pr-sm-0">
                        <img class="rounded-circle" src="{{ $magazine['is_admin'] == 1 ? asset('assets/images/user/admin-1.jpg') : $magazine['user']['photo'] }}" alt="" srcset="" width="45px" height="45px" style="float: right">
                    </div>
                    <div class="col-sm-11 col-10">
                        <b>{{ $magazine['is_admin'] == 0 ? $magazine['user']['fullname'] : 'Admin Rumah Rehat' }}</b>
                        <label class="ml-1" style="margin-top: 4px">{{ $magazine['is_admin'] == 0 ? $magazine['user']['username'] : ''}} 
                            <span class="ml-1" title="Tanggal Dibuat">| <i class="mdi mdi-timer"></i> : {{ $magazine['created_at'] }}</span>
                            <span class="ml-1" title="Terakhir Diperbarui">| <i class="mdi mdi-update"></i> : {{ $magazine['updated_at'] }}</span>
                        </label>
                        <hr class="mt-0">
                    </div>
                </div>
                <div class="row mt-0">
                    <div class="col-md-1"></div>
                    <div class="col-sm-12 col-md-11">
                        {{-- <p style="white-space: pre-line">{{ $magazine['content'] }}</p> --}}
                        {!! $magazine['content'] !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="card pb-2">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;">User Like </span>
                    </div>
                    <div class="col-6">
                        <span style="font-size: 18px;float: right;font-family: sans-serif;color: #1e97ff;text-shadow: 1px 2px 10px #2a2fd838;"><i class="mdi mdi-thumb-up-outline"></i> {{ $magazine['user_like_count'] }}</span>
                    </div>
                </div>
            </span>
            <div class="card-body pt-0 px-1" style="height: 300px" id="likes-height">
                <div style="width:100%;display: block;height: inherit;overflow-y: scroll;">
                <table class="table table-hover table-sm mb-0" cellspacing="0" width="100%">
                    <thead>
                        <tr style="line-height: 2.2">
                            <th style="border-top: none;">NO.</th>
                            <th style="border-top: none;">NAMA</th>
                            <th style="border-top: none;"><center>TANGGAL</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($magazine['user_like'] as $like)
                            <tr>
                                <td style="width: 5px;text-align: center">{{ $loop->iteration }}</td>
                                <td><a href="{{ url('/user').'/'.$like['id'] }}" data-placement="top" data-original-title="Profil">{{ $like['fullname'] }}</a></td>
                                <td><center>{{ date('Y-m-d H:i:s', strtotime($like['created_at'])) }}</center></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3"><center>Belum ada yang menyukai Magazine ini</center></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <span class="card-title p-3"> 
        <div class="row">
            <div class="col-12">
                <span class="ml-2" style="font-size: 18px;" >Daftar Komentar <span style="font-size: 18px;font-family: sans-serif;color: #1e97ff;text-shadow: 1px 2px 10px #2a2fd838;"><i class="mdi mdi-comment-outline"></i> {{ $magazine['comments_count'] }}</span></span>
            </div>
        </div>
    </span>
    <div class="card-body pt-0" >
        <table class=" table table-hover table-sm" width="100%" >
            <thead>
                <tr>
                    <th style="border-top: none;">NO.</th>
                    <th style="border-top: none;"></th>
                    <th style="border-top: none;">KOMENTAR</th>
                    <th style="border-top: none;"><center>APPROVE</center></th>
                    <th style="border-top: none;"><center>TANGGAL</center></th>
                    <th style="border-top: none;"></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($magazine['comments'] as $comment)
                    <tr>
                        <td style="width: 10px">{{ $loop->iteration }}</td>
                        <td class="px-0" style="width: 10px"><img class="rounded-circle" src="{{ $comment['user']['photo'] }}" alt="" srcset="" width="40px" height="40px" style="float: right"></td>
                        <td>
                            <b>{{ $comment['user']['fullname'] }}</b><label class="ml-1" style="margin-top: 1px"> {{ $comment['user']['username'] }} </label>
                            <br>
                            <label>{{ $comment['comment'] }}</label>
                        </td>
                        <td>
                            @if ($comment['is_approve'] == 1)
                                <center><label class="label label-rounded label-info">Diperbolehkan</label></center>
                            @else
                                <center><label class="label label-rounded label-warning">Ditangguhkan</label></center>
                            @endif
                        </td>
                        <td style="width: 110px"><center>{{ date('Y-m-d H:i:s', strtotime($comment['created_at'])) }}</center></td>
                        <td style="width: 205px"><center>
                            @if ($comment['is_approve'] == 0)
                                <button class="btn btn-sm btn-info" onclick="changeStatusComment({{ $comment['id'] }}, 1)"><i class="mdi mdi-update"></i> Izinkan</button>
                                @else
                                <button class="btn btn-sm btn-warning" onclick="changeStatusComment({{ $comment['id'] }}, 0)"><i class="mdi mdi-update"></i> Tangguhkan</button>
                            @endif
                            <button class="btn btn-sm btn-danger ml-2" onclick="deleteComment({{ $comment['id'] }})"><i class="mdi mdi-delete"></i> Delete</button>
                            </center>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5"><center>Belum ada komentar tentang magazine ini</center></td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-status-magazine">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin Ingin Mengubah Status Magazine Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                @if ($magazine['is_approve'] == 0)
                    <a href="{{ url('magazine').'/'.$magazine['id'].'/1' }}" type="button" class="btn btn-primary" id="btn-status-magazine">Setujui</a>
                @else
                    <a href="{{ url('magazine').'/'.$magazine['id'].'/0' }}" type="button" class="btn btn-warning" id="btn-status-magazine">Tangguhkan</a>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-update-magazine">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Konten Magazine</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal m-t-10" id="form-magazine" >
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <div class="form-group">
                                <label>Judul Magazine</label>
                                <input type="text" class="form-control" form="form-magazine" name="title" value="{{ $magazine['title'] }}">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Isi Magazine</label>
                                <textarea class="form-control tinymce-editor" rows="5" cols="40" form="form-magazine" name="content" id="test">{{ $magazine['content'] }}</textarea>
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            {{-- <div class="form-group">
                                <label>Status Magazine</label>
                                <select class="form-control" form="form-magazine" name="is_approve">
                                    <option value="0" {{ $magazine['is_approve'] == 0 ? 'selected' : '' }}>Belum Disetujui (Ditangguhkan)</option>
                                    <option value="1" {{ $magazine['is_approve'] == 1 ? 'selected' : '' }}>Telah Disetujui</option>
                                </select>
                                <span class="help-block" style="float: right;color:red"></span>
                            </div> --}}
                            <div class="form-group">
                                <label>Foto Sampul</label><span style="float: right;color:#4c51e6;">*bisa dikosongi jika tidak ada perubahan foto sampul</span>
                                <input type="file" class="form-control" form="form-magazine" name="photo">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success" id="btn-update-magazine">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-magazine">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin Ingin Menghapus Data Magazine Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete-magazine">Hapus</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-update-comment">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-id-comment">
                <input type="hidden" id="input-status-comment">
                <p>Apakah anda yakin ingin merubah status komentar ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn" id="btn-update-comment">Update</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-comment">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-delete">
                <p>Apakah Anda Yakin Ingin Menghapus Komentar Magazine Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete-comment">Hapus</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/libs/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>

<script>

    tinymce.init({
        selector: 'textarea.tinymce-editor',
        init_instance_callback : function(editor) {
            var freeTiny = document.querySelector('.tox .tox-notification--in');
            freeTiny.style.display = 'none';
        },
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        height: 500,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount',
            'quickbars'
        ],
        toolbar: 'undo redo | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | quickimage',
        content_css: '//www.tiny.cloud/css/codepen.min.css'
    });

    $(document).ready(function () {
        setTimeout(function() {
            $("#likes-height").height($("#content-height").height()+10);
        }, 300);
    });

    $('#btn-update-magazine').click(function () {
        let button = $(this);
        emptyFormInput();

        let formData = new FormData($('#form-magazine')[0]);
        $.ajax({    
            type: "post",
            url: "{{ url('magazine').'/'.$magazine['id'] }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                button.html('Menyimpan Data...');
                button.prop('disabled', true);
            },
            success : function(data) {
                // console.log(data);
                if (!data.error) {
                    location.reload();
                } else {
                    for (let key of Object.keys(data.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(data.error[key]);
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                    button.html('Simpan');
                    button.prop('disabled', false);
                }
            }
        });
    });

    $('#btn-delete-magazine').click(function () { 
        let button = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('magazine').'/'.$magazine['id'].'/delete' }}",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend:function () {
                button.html('Menghapus ...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    // location.reload();
                    history.back();
                } else {
                    alert(response.error);
                    button.html('Hapus');
                    button.prop('disabled', false);
                }
            }
        });
    });

    function changeStatusComment(commentId, commentStatus) {
        // let commentId = $(this).data('id');
        // let commentStatus = $(this).data('status');
        $('#input-id-comment').val(commentId);
        $('#input-status-comment').val(commentStatus);
        if (commentStatus == 0) {
            $('#btn-update-comment').addClass('btn-warning').removeClass('btn-info');
        } else {
            $('#btn-update-comment').addClass('btn-info').removeClass('btn-warning');
        };

        $('#modal-update-comment').modal('show');
    };

    $('#btn-update-comment').click(function() {
        let btn = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('magazineComment') }}/"+$('#input-id-comment').val()+"/"+$('#input-status-comment').val(),
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend:function () {
                btn.html('Memperbarui...');
                btn.prop('disabled', true);
            },
            success: function (response) {
                console.log(response);
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    btn.html('Update');
                    btn.prop('disabled', false);
                }
            }
        });
    })

    function deleteComment(commentId) {
        $('#input-delete').val(commentId);
        $('#modal-delete-comment').modal('show');
    }

    $('#btn-delete-comment').click(function() {
        // alert($('#input-delete').val());
        let button = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('magazineComment') }}/"+$('#input-delete').val()+"/delete",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend:function () {
                button.html('Menghapus...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    button.html('Hapus');
                    button.prop('disabled', false);
                }
            }
        });
    });
</script>

@endsection