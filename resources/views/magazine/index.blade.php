@extends('template/layout')

@section('title', 'Magazine')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh magazine yang akan ditampilkan untuk seluruh pengguna mobile yang terdaftar. Data magazine termasuk dari admin maupun dari user dengan status sudah disetujui ataupun belum disetujui</h6>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary m-b-10" data-toggle="modal" data-target="#modal-add-magazine" style="float: right;"><i class="mdi mdi-plus"></i> Tambah Magazine</button>
                    </div>
                </div>
                {{-- <h6 class="card-title m-t-40"><i class="m-r-5 font-18 mdi mdi-numeric-1-box-multiple-outline"></i> Table With Outside Padding</h6>  --}}
                <div class="table-responsive">
                    <table id="table-magazine" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>TITLE</th>
                                <th>AUTHOR</th>
                                <th>APPROVE</th>
                                <th style="text-align: center">DIBUAT</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- <tr>
                                <td style="width: 5px;text-align: center">2</td>
                                <td>Kategori Kedua</td>
                                <td>Deskripsi untuk Kategori Kedua</td>
                                <td style="text-align: center;width: 170px">21-04-2020 07:15:43</td>
                                <td style="text-align: center;width: 170px">25-04-2020 14:01:21</td>
                                <td style="width: 165px">
                                    <a href="#" class="btn btn-success btn-sm" data-placement="top" data-original-title="Detail"><i class="mdi mdi-magnify"></i> Detail</a>
                                    <button class="btn btn-sm btn-danger m-l-5" onclick="deleteGroup()" ><i class="mdi mdi-delete"></i> Delete</button>
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-add-magazine">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambahkan Magazine</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal m-t-10" id="form-magazine" >
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <div class="form-group">
                                <label>Judul Magazine</label>
                                <input type="text" class="form-control" form="form-magazine" name="title">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Isi Magazine</label>
                                <textarea class="form-control tinymce-editor" rows="5" cols="40" form="form-magazine" name="content" id="test"></textarea>
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                            <div class="form-group">
                                <label>Foto Sampul</label>
                                <input type="file" class="form-control" form="form-magazine" name="photo">
                                <span class="help-block" style="float: right;color:red"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success" id="btn-add-magazine">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-delete">
                <p>Apakah Anda Yakin Ingin Menghapus Data Pasien Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/libs/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: 'textarea.tinymce-editor',
        init_instance_callback : function(editor) {
            let freeTiny = document.querySelector('.tox .tox-notification--in');
            freeTiny.style.display = 'none';
        },
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        height: 500,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount',
            'quickbars'
        ],
        toolbar: 'undo redo | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | quickimage',
        content_css: '//www.tiny.cloud/css/codepen.min.css'
    });

    $(document).ready(function () {
        let t = $('#table-magazine').DataTable( {
            "pageLength": 50,
            "processing": true,
            // "serverSide": true,
            "ajax": "{{ url('/getAllMagazine') }}",
            "columns": [
                {
                    "data": null,
                    "width": "20px",
                    "sClass": "text-center",
                    "bSort": false
                },
                { "data": "title"},
                { "data": "null", render: function ( data, type, row ) {
                        if (row.is_admin == 1) {
                            return 'admin';
                        } else {
                            return row.user.fullname;
                        }
                    } 
                },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        if (row.is_approve == 1) {
                            return '<label class="label label-rounded label-info">Disetujui</label>';
                        } else {
                            return '<label class="label label-rounded label-warning">Ditangguhkan</label>';
                        };
                    } 
                },
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", render: function ( data, type, row ) {
                        return `
                            <a href="{{ url('magazine') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i> Detail</a>
                            <button class="btn btn-sm btn-danger m-l-5" onclick="deleteMagazine(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                            `;
                    },"bSortable": false
                }
            ],
            "createdRow": function (row, data, index) {
                // $('td', row).eq(1).css('color', 'red');
                $('td', row).eq(1).addClass('truncated-magazine');
                // $('td', row).eq(2).css('width', '80px');
                // $('td', row).eq(3).css('width', '90px');
                // $('td', row).eq(4).css('width', '80px');
                $('td', row).eq(5).css('width', '150px');
            },
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    $('#btn-add-magazine').click(function () {
        let button = $(this);
        emptyFormInput();

        let formData = new FormData($('#form-magazine')[0]);
        $.ajax({    
            type: "post",
            url: "{{ url('magazine') }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                button.text('Menyimpan Data ...');
                button.prop('disabled', true);
            },
            success : function(data) {
                // console.log(data);
                if (!data.error) {
                    location.reload();
                } else {
                    for (let key of Object.keys(data.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().text(data.error[key]);
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                }
            },
            complete:function(data) {
                button.text('Simpan');
                button.prop('disabled', false);
            }
        });
    });

    function deleteMagazine(magazineId) {
        $('#input-delete').val(magazineId);
        $('#modal-delete').modal('show');
    }

    $('#btn-delete').click(function() {
        let button = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('magazine') }}/"+$('#input-delete').val()+"/delete",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend:function () {
                button.text('Menghapus...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    button.text('Hapus');
                    button.prop('disabled', false);
                }
            }
        });
    });

</script>

@endsection