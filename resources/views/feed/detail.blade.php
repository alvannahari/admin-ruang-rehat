@extends('template.layout')

@section('title')
    Detail Feed
@endsection

@section('content')
<div class="row">
    <div class="col-12 col-lg-7">
        <div class="card" style="min-height: 300px;height: fit-content;">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-3">
                        <span style="font-size: 18px;">Konten </span>
                    </div>
                    <div class="col-9">
                        <button class="btn btn-sm btn-danger" style="float: right" data-toggle="modal" data-target="#modal-delete-post"> <i class="mdi mdi-delete"></i> Hapus Post</button>
                        @if ($feed['is_reported'] == 0)
                            <button type="button" class="btn btn-sm btn-warning m-r-15" style="float: right" data-toggle="modal" data-target="#modal-status-post"> <i class="mdi mdi-update"></i> Tangguhkan/Laporkan</button>
                        @else
                            <button type="button" class="btn btn-sm btn-primary m-r-15" style="float: right" data-toggle="modal" data-target="#modal-status-post"> <i class="mdi mdi-update"></i> Izinkan</button>
                        @endif
                    </div>
                </div>
            </span>
            <div class="card-body" id="content-height">
                <div class="row">
                    <div class="col-3 ">
                        <img class="rounded-circle" src="{{ $feed['user']['photo'] }}" alt="" srcset="" width="70px" height="70px" style="float: right">
                    </div>
                    <div class="col-9 ">
                        <b>{{ $feed['user']['fullname'] }}</b><label class="ml-1 mb-0" style="margin-top: 4px">{{ $feed['user']['username'] }} <i class="mdi mdi-timer ml-2">' </i><font style="font-size: 13px">{{ $feed['created_at'] }}</font></label>
                        <div class="row px-2">
                            <label class="label label-rounded label-info" style="padding-top: 6px;">
                                <i class="mdi mdi-check-circle-outline" style="font-size: 17px"></i> 
                                <span class="ml-1" style="float:right">anonymous</span>
                            </label>
                        </div>
                        <p style="white-space: pre-line">{{ $feed['content'] }}</p>
                    </div>
                </div>
                {{-- <div class="row pl-4 pr-1">
                    <div class="col-4 p-1"><button class="btn btn-block" disabled><i class="mdi mdi-comment"></i> {{ $feed['comments_count'] }} Komentar</button></div>
                    <div class="col-4 p-1"><button class="btn btn-block px-0" disabled><i class="mdi mdi-timer"></i><font style="font-size: 14px"> {{ $feed['created_at'] }}</font></button></div>
                    <div class="col-4 p-1"><button class="btn btn-block px-0" disabled><i class="mdi mdi-update"></i><font style="font-size: 14px"> {{ $feed['updated_at'] }}</font></button></div>
                    <div class="col-4 p-1"><button class="btn btn-block px-0" disabled><i class="mdi mdi-update"></i><font style="font-size: 13px"> {{ date('Y-m-d', strtotime($feed['updated_at'])) }}</font></button></div>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-5">
        <div class="card">
            <span class="card-title p-3"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;">Pelapor</span>
                    </div>
                    <div class="col-6">
                        <span style="font-size: 18px;float: right;font-family: sans-serif;color: #1e97ff;text-shadow: 1px 2px 10px #2a2fd838;"><i class="mdi mdi-account-alert"></i> {{ $feed['reports_count'] }}</span>
                    </div>
                </div>
            </span>
            <div class="card-body pt-0" >
                <div style="width:100%;display: block;overflow-y: scroll;" id="reports-height">
                    <table class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th style="border-top: none;">NO.</th>
                                <th style="border-top: none;">NAMA</th>
                                <th style="border-top: none;">TANGGAL</th>
                                <th style="border-top: none;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($feed['reports'] as $report)
                                <tr>
                                    <td style="width: 5px;text-align: center">{{ $loop->iteration }}</td>
                                    <td>{{ $report['user']['fullname'] }}</td>
                                    <td>{{ $report['created_at'] }}</td>
                                    <td><button class="btn btn-sm btn-info" onclick="viewReport({{ $loop->index }})"><i class="mdi mdi-eye-outline"></i></button></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">Belum ada yang melaporkan post ini</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <span class="card-title p-3"> 
        <div class="row">
            <div class="col-6">
                <span style="font-size: 18px;">Daftar Komentar <span style="font-size: 18px;font-family: sans-serif;color: #1e97ff;text-shadow: 1px 2px 10px #2a2fd838;"><i class="mdi mdi-comment-outline"></i> {{ $feed['comments_count'] }}</span></span>
            </div>
        </div>
    </span>
    <div class="card-body pt-0" >
        <table class=" table table-hover table-sm" width="100%" >
            <thead>
                <tr>
                    <th style="border-top: none;">NO.</th>
                    <th style="border-top: none;"></th>
                    <th style="border-top: none;">KOMENTAR</th>
                    <th style="border-top: none;"><center>REPORT</center></th>
                    <th style="border-top: none;"><center>TANGGAL</center></th>
                    <th style="border-top: none;"></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($feed['comments'] as $comment)
                    <tr>
                        <td style="width: 5px"><center>{{ $loop->iteration }}</center></td>
                        <td style="width: 10px"><img class="rounded-circle" src="{{ $comment['user']['photo'] }}" alt="" srcset="" width="40px" height="40px" style="float: right"></td>
                        <td>
                            <b>{{ $comment['user']['fullname'] }}</b><label class="ml-1" style="margin-top: 1px"> {{ $comment['user']['username'] }} </label>
                            <p>{{ $comment['comment'] }}</p>
                        </td>
                        <td><center>
                            @if ($comment['count_reports'] > 0)
                                <label class="label label-rounded label-warning">{{ $comment['count_reports'] }}</label>
                            @else
                            <label class="label label-rounded label-megna">0</label>
                            @endif
                        </center></td>
                        <td><center>{{ $comment['created_at'] }}</center></td>
                        <td style="width: 210px"><center>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%;max-width: 130px;">
                                    Action
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <div class="form-group mx-2">
                                        <button class="btn btn-sm btn-info" style="width: 100%;text-align:left" onclick="viewReportComments({{ $loop->index }})"><i class="mdi mdi-eye-outline"></i> Lihat Report</button>
                                    </div>
                                    <div class="form-group mx-2">
                                    @if ($comment['is_reported'] == '0')
                                        <button class="btn btn-sm btn-warning" style="width: 100%;text-align:left" onclick="statusComment({{ $comment['id'] }})"><i class="mdi mdi-update"></i> Tangguhkan</button>
                                    @else
                                        <button class="btn btn-sm btn-primary" style="width: 100%;text-align:left" onclick="statusComment({{ $comment['id'] }})"><i class="mdi mdi-update"></i> Izinkan</button>
                                    @endif
                                    </div>
                                    <div class="form-group mx-2 mb-0">
                                        <button class="btn btn-sm btn-danger" style="width: 100%;text-align:left" onclick="removeComment({{ $comment['id'] }})"><i class="mdi mdi-delete"></i> Hapus</button>
                                    </div>
                                </div>
                            </div>
                            
                        </center></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">Belum ada komentar tentang post ini</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-status-post">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if ($feed['is_reported'] == 0)
                    <p>Apakah Anda Yakin Ingin Melaporkan Post Ini ? </p>
                @else
                    <p>Apakah Anda Yakin Ingin Mengizinkan Post Ini ? </p>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                @if ($feed['is_reported'] == 0)
                    <button class="btn btn-warning" id="btn-status-post">Tangguhkan</button>
                @else
                    <button class="btn btn-primary" id="btn-status-post">Izinkan</button>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-post">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin Ingin Menghapus Data Post Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete-post">Hapus</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-view-report">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Laporan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-4"><label><b>Nama Lengkap</b></label></div>
                        <div class="col-1"><label><b>:</b></label></div>
                        <div class="col-7"><label id="view-fullname-report"></label></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4"><label><b>Username</b></label></div>
                        <div class="col-1"><label><b>:</b></label></div>
                        <div class="col-7"><label id="view-username-report"></label></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4"><label><b>Email</b></label></div>
                        <div class="col-1"><label><b>:</b></label></div>
                        <div class="col-7"><label id="view-email-report"></label></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4"><label><b>Alasan</b></label></div>
                        <div class="col-1"><label><b>:</b></label></div>
                        <div class="col-7"><label id="view-argument-report"></label></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4"><label><b>Tanggal Laporan</b></label></div>
                        <div class="col-1"><label><b>:</b></label></div>
                        <div class="col-7"><label id="view-date-report"></label></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-view-report-comments">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Laporan Komentar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm" width="100%">
                    <thead class="thead-light">
                        <tr>
                            <th><center>No.</center></th>
                            <th>Nama Lengkap</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Alasan</th>
                            <th><center>Tanggal</center></th>
                        </tr>
                    </thead>
                    <tbody id="table-view-comment-reports">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-status-comment">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-status-comment">
                @if ($feed['is_reported'] == 0)
                    <p>Apakah Anda Yakin Ingin Melaporkan Komentar Post Ini ? </p>
                @else
                    <p>Apakah Anda Yakin Ingin Mengizinkan Komentar Post Ini ? </p>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                @if ($feed['is_reported'] == 0)
                    <button class="btn btn-warning" id="btn-status-comment">Tangguhkan</button>
                @else
                    <button class="btn btn-primary" id="btn-status-comment">Izinkan</button>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-comment">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-delete-comment">
                <p>Apakah Anda Yakin Ingin Menghapus Komentar Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger" id="btn-delete-comment">Hapus</button>
            </div>
        </div>
    </div>
</div>


<script>
    let dataReports = {!! json_encode($feed['reports']) !!};

    let dataComments = {!! json_encode($feed['comments']) !!};

    $(document).ready(function () {
        setTimeout(function() {
            $("#reports-height").height($("#content-height").height()+20);
            // alert($("#content-height").height());
        }, 300);
    });

    $('#btn-status-post').click(function() {
        let button = $(this);
        button.html('Menghapus ...');
        button.prop('disabled', true);
        window.location = "{{ url('reportFeed').'/'.$feed['id'] }}";
    });

    $('#btn-delete-post').click(function () {
        let button = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('feed').'/'.$feed['id'].'/delete' }}",
            beforeSend:function () {
                button.html('Menghapus ...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    // location.reload();
                    history.back();
                } else {
                    alert(response.error);
                    button.html('Hapus');
                    button.prop('disabled', false);
                }
            }
        });
    });

    function viewReport(index) {
        $('#modal-view-report').modal('show');
        $('#view-fullname-report').html(dataReports[index]['user']['fullname']);
        $('#view-username-report').html(dataReports[index]['user']['username']);
        $('#view-email-report').html(dataReports[index]['user']['email']);
        $('#view-argument-report').html(dataReports[index]['argument']);
        $('#view-date-report').html(dataReports[index]['created_at']);
    }

    function viewReportComments(index) {
        $('#modal-view-report-comments').modal('show');
        let elCommentReports = '';
        dataComments[index]['reports'].forEach(function (item, index) {
            let number = index+1;
            elCommentReports += `
                <tr>
                    <td><center>`+number+`</center></td>
                    <td>`+item.user.fullname+`</td>
                    <td>`+item.user.username+`</td>
                    <td>`+item.user.email+`</td>
                    <td>`+item.argument+`</td>
                    <td><center>`+item.created_at+`</center></td>
                </tr>
            `;
        });
        $('#table-view-comment-reports').html(elCommentReports);
    }

    function statusComment(commentId) {
        $('#input-status-comment').val(commentId);
        $('#modal-status-comment').modal('show');
    }

    $('#btn-status-comment').click(function() {
        let button = $(this);
        button.html('Mengganti...');
        button.prop('disabled', true);
        window.location = "{{ url('reportFeedComment') }}"+"/"+$('#input-status-comment').val();
    });

    function removeComment(commentId) {
        $('#input-delete-comment').val(commentId);
        $('#modal-delete-comment').modal('show');
    }

    $('#btn-delete-comment').click(function() {
        let button = $(this);
        button.html('Menghapus ...');
        button.prop('disabled', true);
        window.location = "{{ url('feedComment') }}/"+$('#input-delete-comment').val()+"/delete";
        // $.ajax({
        //     type: "get",
        //     url: "{{ url('feedComment') }}/"+$('#input-delete-comment').val()+"/delete",
        //     beforeSend:function () {
        //         button.html('Menghapus...');
        //         button.prop('disabled', true);
        //     },
        //     success: function (response) {
        //         if (!response.error) {
        //             location.reload();
        //         } else {
        //             alert(response.error);
        //             button.html('Hapus');
        //             button.prop('disabled', false);
        //         }
        //     }
        // });
    });
    
    

</script>
@endsection