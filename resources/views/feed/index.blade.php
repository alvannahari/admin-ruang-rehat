@extends('template/layout')

@section('title', 'Feed')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-category" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>FEED</th>
                                <th>REPORT</th>
                                <th>SUSPENDED</th>
                                <th style="text-align: center">DIBUAT</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input-delete">
                <p>Apakah Anda Yakin Ingin Menghapus Data Post Ini ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger" id="btn-delete">Hapus</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
        let t = $('#table-category').DataTable( {
            "pageLength": 50,
            "processing": true,
            // "serverSide": true,
            "ajax": "{{ url('/getAllfeeds') }}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSort": false
                },
                { "data": "content"},
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.reports_count == 0) 
                        return'<label class="label label-rounded label-megna">'+row.reports_count+'</label>';
                    else if (row.reports_count > 10) 
                        return'<label class="label label-rounded label-danger">'+row.reports_count+'</label>';
                    else if (row.reports_count > 0) 
                        return '<label class="label label-rounded label-warning">'+row.reports_count+'</label>';
                    } 
                },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        if (row.is_reported == '0') {
                            return '<label class="label label-rounded label-info">Tidak</label>';
                        } else {
                            return '<label class="label label-rounded label-warning">Ya</label>';
                        };
                    } 
                },
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return `
                            <a href="{{ url('feed') }}/`+row.id+`" class="btn btn-sm btn-success"><i class="mdi mdi-magnify"></i> Detail</a>
                            <button class="btn btn-sm btn-danger m-l-5" onclick="deleteFeed(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                            `;
                    },"bSortable": false
                }
            ],
            "createdRow": function (row, data, index) {
                $('td', row).eq(1).addClass('truncated');
                // $('td', row).eq(2).css('width', '50px');
                // $('td', row).eq(3).css('width', '40px');
                // $('td', row).eq(4).css('width', '80px');
                $('td', row).eq(5).css('width', '150px');
                // $('td', row).eq(5).css('width', '120px');
                // $('td', row).eq(1).css('overflow', 'hidden');
                // $('td', row).eq(1).css('text-overflow', 'ellipsis');
                // $('td', row).eq(1).css('white-space', 'nowrap');
            },
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    function deleteFeed(feedId) {
        $('#input-delete').val(feedId);
        $('#modal-delete').modal('show');
    }

    $('#btn-delete').click(function() {
        let btn = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('feed') }}/"+$('#input-delete').val()+"/delete",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            beforeSend:function () {
                btn.html('Menghapus ...');
                btn.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    btn.html('Hapus');
                    btn.prop('disabled', false);
                }
            }
        });
    });

</script>

@endsection