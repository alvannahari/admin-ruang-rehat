@extends('template/layout')

@section('title', 'Dashboard')

@section('content')
<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="card">
            <div class="card-body" style="align-items: center">
                <span class="card-title pt-3" style="border-bottom: none"> 
                <span style="font-size: 18px;">Aktivitas Post Pengguna </span>
                </span>
                <canvas class="mt-2" id="chart-user" style="width:100%" height="118">
            </div>
        </div>
    </div>
    <!-- column -->
    <div class="col-md-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <center>
                            <span class="card-title" style="border-bottom: none"> 
                            <span style="font-size: 18px;">Status Feed</span>
                            </span>
                        </center>
                        <canvas class="mt-2" id="chart-learning" width="250" height="200" style="display: inline-block;height: auto;width: inherit;"></canvas>
                    </div>
                    <div class="col-6" >
                        <center>
                            <span class="card-title" style="border-bottom: none"> 
                            <span style="font-size: 18px;">Status Magazine</span>
                            </span>
                        </center>
                        <canvas class="mt-2" id="chart-kognitif" width="250" height="200" style="display: inline-block;height: auto;width: inherit;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- column -->
    <div class="col-12">
        <div class="card">
            <span class="card-title pt-3 px-3" style="border-bottom: none"> 
                <span style="font-size: 18px;">Riwayat Aktivitas Post Pengguna</span>
            </span>
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="border-top-0" style="width: 5px">NO.</th>
                                <th class="border-top-0" style="min-width: 150px">NAMA PENGGUNA</th>
                                <th class="border-top-0">KONTEN</th>
                                <th class="border-top-0"><center>ANONYMOUS</center></th>
                                <th class="border-top-0" style="width: 170px"><center>TANGGAL WAKTU</center></th>
                                <th class="border-top-0" style="width: 70px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($data['histories'] as $history)
                            <tr>
                                <td><span class="font-medium"><center>{{ $loop->iteration }}</center></span></td>
                                <td class="txt-oflo">{{ $history['user']['fullname'] }}</td>
                                <td class="txt-oflo">{{ $history['content'] }}</td>
                                <td class="txt-oflo"><center>
                                    @if ($history['is_anonymous'] == 0)
                                    <label class="label label-rounded label-warning">Ya</label>
                                    @else
                                        <label class="label label-rounded label-info">Tidak</label>
                                    @endif
                                </center></td>
                                <td class="txt-oflo"><center>{{ $history['created_at'] }}</center></td>
                                <td class="txt-oflo"><center><a href="{{ url('feed').'/'.$history['id'] }}" class="btn btn-sm btn-primary"><i class="mdi mdi-magnify"></i> Detail</a></center></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4"><center>Belum Ada Aktivitas Pengguna</center></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
    var sum = function(a, b) { return a + b; };

    var labelActivity = {!!json_encode($data['activity']['label'])!!};
    var dataActivity = {!!json_encode($data['activity']['value'])!!};

    var ctxUser = $("#chart-user")[0].getContext("2d");
    var chartUser = new Chart(ctxUser, {
        type: 'line',
        data: {
            datasets: [{
                data: dataActivity,
                backgroundColor: 'rgba(41, 182, 246,0.2)',
                borderColor: 'rgba(41, 182, 246, 1)',
                hoverBackgroundColor : 'white',
                borderWidth: 2,
                label : 'Jumlah Siswa Aktif'
            }],
            labels: labelActivity
        },
        options: {
            animation: {
                animateScale: true,
                animateRotate: true
            },
            responsive: true,
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                            return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    var ctxLearning = $("#chart-learning")[0].getContext("2d");
    let dataFeed = {!!json_encode($data['feed'])!!};
    var chartLearning = new Chart(ctxLearning, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: dataFeed.total,
                backgroundColor: ['#4FC3F7', '#FFEE58'],
                borderWidth: 1
            }],
            labels: dataFeed.label,
        },
        options: {
            animation: {
                animateScale: true,
                animateRotate: true
            },
            responsive: false,
            // maintainAspectRatio: true,
            cutoutPercentage: 1,
            plugins: {
                datalabels: {
                    color: 'black',
                    formatter: function(value, context) {
                        return Math.round(value / context.chart.data.datasets[0].data.reduce(sum) * 100) + '%';
                    }
                }
            },
            legend: {
                display: true, 
                position:'bottom',
                labels: {
                    padding : 12,
                    fontColor: 'black',
                    fontSize : 11,
                    boxWidth: 20,
                    boxHeight: 2,
                },
            }
        }
    });

    let dataMagazine = {!!json_encode($data['magazine'])!!};
    var ctxKognitif = $("#chart-kognitif")[0].getContext("2d");
        var chartKognitif = new Chart(ctxKognitif, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: dataMagazine.total,
                    backgroundColor: ['#F44336', '#66BB6A'],
                    borderWidth: 1
                }],
                labels: dataMagazine.label
            },
            options: {
                plugins: {
                    datalabels: {
                        color: 'black',
                        formatter: function(value, context) {
                            return Math.round(value / context.chart.data.datasets[0].data.reduce(sum) * 100) + '%';
                        }
                    }
                },
                responsive: false,
                cutoutPercentage: 1,
                legend: {
                    display: true, 
                    position:'bottom',
                    labels: {
                        padding : 12,
                        fontColor: 'black',
                        fontSize : 11,
                        boxWidth: 20,
                        boxHeight: 2,
                    },
                }, 
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            },
        });
  });
</script>
@endsection